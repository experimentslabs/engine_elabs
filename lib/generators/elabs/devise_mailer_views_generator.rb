# frozen_string_literal: true

require 'rails/generators/base'

module Elabs
  module Generators
    class DeviseMailerViewsGenerator < Rails::Generators::Base
      source_root File.expand_path('../../..', __dir__)

      def set_source_paths
        @source_paths = [
          File.expand_path('../templates', __dir__),
          File.expand_path('../../..', __dir__)
        ]
      end

      desc 'Copies devise mailer views in your app'

      def copy_devise_views
        directory 'spec/dummy/app/views/devise', 'app/views/devise'
      end
    end
  end
end
