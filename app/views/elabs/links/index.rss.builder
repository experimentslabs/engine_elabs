xml.instruct! :xml, version: '1.0'
xml.rss version: '2.0' do
  xml.channel do
    xml.title "#{Elabs.site_name} - links"
    xml.description "Last #{params['show_nsfw'] == 'false' ? 'SFW' : ''} links from #{Elabs.site_name}"
    xml.link links_url

    @links.each do |link|
      next if params['show_nsfw'] == 'false' && !link.sfw

      render 'link', builder: xml, link: link
    end
  end
end
