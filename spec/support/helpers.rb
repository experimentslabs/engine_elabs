module Helpers
  def create_user_with_published_content
    user     = FactoryBot.create :user_active
    language = FactoryBot.create :language
    license  = FactoryBot.create :license
    tag      = FactoryBot.create :tag
    project  = FactoryBot.create :project_published, user: user, language: language, license: license, tags: [tag]
    album = FactoryBot.create :album_published, user: user, language: language, license: license, tags: [tag], projects: [project]
    FactoryBot.create :article_published, user: user, language: language, license: license, tags: [tag], projects: [project]
    FactoryBot.create :note_published, user: user, language: language, license: license, tags: [tag], projects: [project]
    FactoryBot.create :upload_published, user: user, language: language, license: license, tags: [tag], projects: [project], albums: [album]

    user.reload
  end

  def create_user_with_unpublished_content
    user     = FactoryBot.create :user_active
    language = FactoryBot.create :language
    license  = FactoryBot.create :license
    tag      = FactoryBot.create :tag
    project  = FactoryBot.create :project, user: user, language: language, license: license, tags: [tag]
    album = FactoryBot.create :album, user: user, language: language, license: license, tags: [tag], projects: [project]
    FactoryBot.create :article, user: user, language: language, license: license, tags: [tag], projects: [project]
    FactoryBot.create :note, user: user, language: language, license: license, tags: [tag], projects: [project]
    FactoryBot.create :upload, user: user, language: language, license: license, tags: [tag], projects: [project], albums: [album]

    user.reload
  end
end
