# Feature terms
This document lists the different terms used in the features for clarity and reference.

User-related terms
- user: someone visiting the website, registered or not
- visitor: unregistered user
- member: registered user
- maintainer: website owner
- administrator: a member with admin rights
