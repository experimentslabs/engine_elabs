Feature: Content tag
  As a maintainer
  In order to allow creators to write in their native tag
  I want to be able to manage licenses attachable to contents

  Background:
    Given I am an administrator
    And a website with a "Creative Commons" license

  Scenario: Admin display licenses
    Given I visit the admin licenses page
    Then I should see a list of licenses

  Scenario: Admin update licenses
    Given I change a license to "LoveLicense"
    Then The license should now have the "LoveLicense" name

  Scenario: Admin create licenses
    When I create the "Free as in beer" license
    Then I should see the "Free as in beer" license in the list

  Scenario: Admin delete tags
    When I delete the "Creative Commons" license
    Then I should see that the "Creative Commons" license is not present in the list
