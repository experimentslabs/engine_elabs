# frozen_string_literal: true

def add_i18n_tasks_use(key)
  @content += "# i18n-tasks-use t('#{key}')\n"
end

def add_i18n_tasks_use_for_activerecord_model(model)
  add_i18n_tasks_use("activerecord.models.#{model}")
end

def add_i18n_tasks_use_for_activerecord_attribute(model, attribute)
  add_i18n_tasks_use("activerecord.attributes.#{model}.#{attribute}")
end

def add_i18n_task_use_for_acts_helper_strings(class_name)
  base_name = class_name.demodulize.underscore
  add_i18n_tasks_use("elabs.acts_helper.act_notice_string.#{base_name}_lock_event")
  add_i18n_tasks_use("elabs.acts_helper.act_notice_string.#{base_name}_unpublish_event")
  add_i18n_tasks_use("elabs.acts_helper.act_notice_string.#{base_name}_update_event")
end

I18N_FILE = File.join('app', 'i18n', 'elabs', 'model_attributes.i18n')
FileUtils.rm(I18N_FILE) if File.exist? I18N_FILE

EXCLUDED_MODELS = [
  'Elabs::AlbumsTag',
  'Elabs::AlbumsUpload',
  'Elabs::ApplicationContentRecord',
  'Elabs::ApplicationRecord',
  'Elabs::ArticlesTag',
  'Elabs::LinksTag',
  'Elabs::NotesTag',
  'Elabs::ProjectsAlbum',
  'Elabs::ProjectsArticle',
  'Elabs::ProjectsNote',
  'Elabs::ProjectsTag',
  'Elabs::ProjectsTag',
  'Elabs::ProjectsUpload',
  'Elabs::UploadsTag'
].freeze

NOT_CONTENT_TYPES = [
  'Elabs::Act',
  'Elabs::Announcement',
  'Elabs::Comment',
  'Elabs::Language',
  'Elabs::License',
  'Elabs::Notification',
  'Elabs::Preference',
  'Elabs::Report',
  'Elabs::Tag',
  'Elabs::User'
].freeze

@content = ''

Dir.glob(File.join('app', 'models', 'elabs', '*.rb')).sort.each do |f|
  class_name = "Elabs::#{File.basename(f, '.rb').camelize}"

  next if EXCLUDED_MODELS.include? class_name

  model = class_name.split('::').join('/').underscore
  Rails.logger.debug "Checking for model #{class_name} (#{model})"
  add_i18n_tasks_use_for_activerecord_model(model)
  add_i18n_task_use_for_acts_helper_strings(class_name) unless NOT_CONTENT_TYPES.include? class_name

  # Standard attributes
  class_obj = class_name.constantize
  class_obj.attribute_types.sort.each do |attribute, _type|
    next if attribute == 'id'

    add_i18n_tasks_use_for_activerecord_attribute(model, attribute.sub(/_id$/, ''))
  end
end

comment = "# Generated using \"rake app:i18n:add-models-attributes\" - Do not modify manually\n"
File.open(I18N_FILE, 'w') do |file|
  file.write "#{comment}\n#{@content}\n#{comment}\n"
end
