FactoryBot.define do
  factory :tag, class: Elabs::Tag do
    name { Faker::Alphanumeric.unique.alpha number: 12 }
  end
end
