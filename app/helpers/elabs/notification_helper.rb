module Elabs
  module NotificationHelper
    def notification_message(notification, links = true)
      user   = notification_user notification, links
      action = notification_action notification.event
      link   = notification_link notification, links
      string = notification_string notification, user: ERB::Util.html_escape(user), action: action, link: link

      # rubocop:disable Rails/OutputSafety
      # Username is escaped
      # Link title is escaped
      string.html_safe
      # rubocop:enable Rails/OutputSafety
    end

    def notification_icon(event)
      actions = {
        comment: 'comment',
        report:  'flag',
        lock:    'lock',
        unlock:  'unlock',
        delete:  'trash'
      }.freeze
      icon (actions[event.to_sym] || 'question'), ['fw']
    end

    def notification_count
      Elabs::Notification.for_user(current_user).count
    end

    private

    def notification_action(event)
      actions = {
        comment: t('elabs.notification_helper.notification_action.has_commented'),
        report:  t('elabs.notification_helper.notification_action.has_reported'),
        lock:    t('elabs.notification_helper.notification_action.has_locked'),
        unlock:  t('elabs.notification_helper.notification_action.has_unlocked'),
        delete:  t('elabs.notification_helper.notification_action.has_deleted')
      }.freeze
      actions[event.to_sym] || "[missing: #{event}]"
    end

    def notification_user(notification, link)
      if ['Elabs::Comment', 'Elabs::Report'].include? notification.content_type
        user = nil
        username = notification.content.username
      else
        user = notification.source_user
        username = user.display_name
      end

      return link_to username, user if user && link
      return username if username

      t('elabs.notification_helper.notification_user.someone')
    end

    def notification_link(notification, link)
      return "##{notification.content_id}" if notification.event == 'delete'

      title = notification_link_title notification

      return title unless link

      case notification.content_type
      when 'Elabs::Report'
        ERB::Util.html_escape admin_reports_url
      when 'Elabs::Comment'
        # rubocop:disable Rails/OutputSafety
        # Internal URL; title is safe
        link_to(title, url_for(notification.content.content)).html_safe
        # rubocop:enable Rails/OutputSafety
      else
        # rubocop:disable Rails/OutputSafety
        # Internal URL; title is safe
        link_to(title, notification.content).html_safe
        # rubocop:enable Rails/OutputSafety
      end
    end

    def notification_link_title(notification)
      return notification.content.title_to_display if notification.content.respond_to?(:title_to_display)

      case notification.content_type
      when 'Elabs::Comment'
        ERB::Util.html_escape notification.content.content.title_to_display
      when 'Elabs::Report'
        t('elabs.notification_helper.notification_link_title.a_page')
      else
        "##{notification.content_id}"
      end
    end

    def notification_string(notification, interpolations)
      strings = {
        'Elabs::Album'   => t('elabs.notification_helper.notification_string.user_did_something_on_album', interpolations),
        'Elabs::Article' => t('elabs.notification_helper.notification_string.user_did_something_on_article', interpolations),
        'Elabs::Note'    => t('elabs.notification_helper.notification_string.user_did_something_on_note', interpolations),
        'Elabs::Project' => t('elabs.notification_helper.notification_string.user_did_something_on_project', interpolations),
        'Elabs::Upload'  => t('elabs.notification_helper.notification_string.user_did_something_on_file', interpolations),
        'Elabs::Report'  => t('elabs.notification_helper.notification_string.user_did_action', interpolations),
        'Elabs::Comment' => t('elabs.notification_helper.notification_string.user_did_action', interpolations)
      }.freeze
      strings[notification.content_type] || t('elabs.notification_helper.notification_string.user_did_something_on_somewhat')
    end
  end
end
