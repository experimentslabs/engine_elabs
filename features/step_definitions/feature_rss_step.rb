When('I check the {string} RSS feed') do |type|
  within '.page__footer' do
    click_on type.capitalize
  end
end

Then('I should see {int} items in the RSS feed') do |amount|
  expect(find_all('//item').count).to eq(amount)
end
