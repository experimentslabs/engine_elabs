Feature: Switch website language
  As a maintainer
  In order to make navigation comfortable
  I want to propose multiple website translations

  Scenario: Change the language
    Given a website with an "fr" language for content in "Français"
    And a website with an "en" language for content in "English"
    When I visit the home page
    Then I should see "Last events"
    When I change language to "Français"
    Then I should see "Derniers évènements"
    When I visit the home page again
    Then I should still see "Derniers évènements"
