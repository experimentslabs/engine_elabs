Given('I set the NSFW preferences to display NSFW content') do
  click_on 'Preferences'

  check 'Show nsfw'

  click_on 'Save'
end

Given('I set the locale preferences to {string}') do |string|
  click_on 'Preferences'

  select string, from: 'Locale'

  click_on 'Save'
end

Given('I set the writing language preference to {string}') do |string|
  click_on 'Preferences'

  select string, from: 'Writing language'

  click_on 'Save'
  expect(page).to have_content('Preferences were successfully updated.')
end

Given('I set the writing license preference to {string}') do |string|
  click_on 'Preferences'

  select string, from: 'Writing license'

  click_on 'Save'
  expect(page).to have_content('Preferences were successfully updated.')
end

Then('the selected language should be {string}') do |string|
  expect(page).to have_select('Language', selected: string)
end

Then('the selected license should be {string}') do |string|
  expect(page).to have_select('License', selected: string)
end
