module Elabs
  class ApplicationMailer < ActionMailer::Base
    default from: Elabs.mailer_default_sender
    layout 'elabs/mailer'
  end
end
