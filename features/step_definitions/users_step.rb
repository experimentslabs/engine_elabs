Given('a website with an active user {string}') do |email|
  FactoryBot.create :user_active, email: email
end

Given('a website with an active user {string} with email {string}') do |username, email|
  FactoryBot.create :user_active, email: email, username: username
end
