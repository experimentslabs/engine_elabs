require 'elabs/engine'

# rubocop:disable Style/ClassVars
module Elabs
  # Site name, used in menus and other places
  mattr_accessor :site_name
  @@site_name = 'A new app'

  # Maximum number of items per public "index" pages
  mattr_accessor :max_items_per_page
  @@max_items_per_page = 15

  # Maximum number of items per admin "index" pages
  mattr_accessor :max_admin_items_per_page
  @@max_admin_items_per_page = 15

  # Maximum number of items per member "index" pages
  mattr_accessor :max_members_items_per_page
  @@max_members_items_per_page = 15

  # Maximum number related content items in "show" views
  mattr_accessor :max_related_items
  @@max_related_items = 10

  # Number of uploads thumbnails to show on albums cards
  mattr_accessor :albums_max_shown_uploads
  @@albums_max_shown_uploads = 3

  # List of audio formats that can be used with html5 players
  mattr_accessor :av_formats_audio
  @@av_formats_audio = ['audio/mpeg', 'audio/ogg', 'audio/wav']

  # List of video formats that can be used with html5 players
  mattr_accessor :av_formats_video
  @@av_formats_video = ['video/mp4', 'video/webm', 'video/ogg']

  # Whether or not to use avatars. This feature is experimental
  mattr_accessor :use_avatars
  @@use_avatars = false

  # Close the registrations
  mattr_accessor :users_can_register
  @@users_can_register = true

  # Enable the form honeypot used for reports/comments
  mattr_accessor :trap_dumb_bots
  @@trap_dumb_bots = true

  # Enable rendering markdown files as download.
  # To use this feature, don't forget to register the markdown file type
  # in "config/initializers/mimetypes.rb":
  #   Mime::Type.register "text/markdown", :md
  mattr_accessor :exportable_markdown
  @@exportable_markdown = true

  # Email address to send notifications from
  mattr_accessor :mailer_default_sender
  @@mailer_default_sender = 'noreply@yoursite.com'

  def self.setup
    yield self
  end

  def self.trap_dumb_bots?
    @@trap_dumb_bots
  end
end
# rubocop:enable Style/ClassVars
