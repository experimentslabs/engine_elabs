// eslint-disable-next-line no-unused-vars
function selectTab (event, tab) {
  if (tab) {
    event.preventDefault()
  } else {
    tab = event
  }

  var i
  var tabs = document.getElementsByClassName('tabs__tabs-list__tab active')
  var tabsContent = document.getElementsByClassName('tabs__tab-content active')

  for (i = 0; i < tabs.length; i++) {
    removeClass(tabs[i], 'active')
  }

  for (i = 0; i < tabsContent.length; i++) {
    removeClass(tabsContent[i], 'active')
  }
  addClass(document.getElementById('tab-content-' + tab), 'active')
  addClass(document.getElementById('tab-' + tab), 'active')
}

// eslint-disable-next-line no-unused-vars
function initializeTabs () {
  var i
  var tabs = document.getElementsByClassName('tabs__tab-content')

  for (i = 0; i < tabs.length; i++) {
    addClass(tabs[i], 'tab--initialized')
  }
}
