module Elabs
  class ProjectsNote < ApplicationRecord
    self.table_name = 'projects_notes'

    include Elabs::Concerns::CountablePivot

    COUNTABLE_DEFINITION = %i[notes project].freeze

    belongs_to :project
    belongs_to :note
  end
end
