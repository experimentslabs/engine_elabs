Feature: Article management
  As a member
  In order to manage the content I publish
  I want to be able to create or update the article's content

  Background:
    Given I am a member with email "bob@example.com"
    And a website with a "Creative Commons by" license
    And a website with a "fr" language for content in "Français"
    And a website with one sfw, published article, "My first article" which I created

  Scenario: Create article
    Given I create an article with title "Cache Invalidation: the easy way"
    Then I should see the article containing "Cache Invalidation: the easy way" in the member zone
    And I should see a list of 2 articles

  Scenario: Update article
    Given I change article "My first article" to "Finding names: how to"
    Then I should see the article containing "Finding names: how to" in the member zone
    And I should see a list of 1 article
