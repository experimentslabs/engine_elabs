# frozen_string_literal: true

require 'rails/generators/base'

module Elabs
  module Generators
    class RequiredAssetsGenerator < Rails::Generators::Base
      source_root File.expand_path('../../..', __dir__)

      def set_source_paths
        @source_paths = [
          File.expand_path('../templates', __dir__),
          File.expand_path('../../..', __dir__)
        ]
      end

      desc 'Copies required files if you plan to use elabs views'

      def copy_files
        template 'spec/dummy/package.json', 'package.json'
      end
    end
  end
end
