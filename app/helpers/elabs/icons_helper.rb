module Elabs
  module IconsHelper
    CONTENT_TYPE_ICONS = {
      'Elabs::Album':   'book',
      'Elabs::Article': 'font',
      'Elabs::Note':    'sticky-note',
      'Elabs::Project': 'cog',
      'Elabs::Upload':  'file'
    }.freeze

    def icon_text(icon, text, icon_options = [], reverse = false)
      options = { icon: icon(icon, icon_options), text: h(text) }
      output = if reverse
                 t('elabs.icons_helper.icon_text.text_icon', options)
               else
                 t('elabs.icons_helper.icon_text.icon_text', options)
               end
      # rubocop:disable Rails/OutputSafety
      # The only possible issue here is if the dev passes HTML in icon name/icon option
      output.html_safe
      # rubocop:enable Rails/OutputSafety
    end

    def boolean_icon_tag(value, true_icon = 'check', false_icon = 'times', false_class = '', true_class = '')
      icon_class = [
        "fas fa-#{value ? true_icon : false_icon} fa-fw #{value ? true_class : false_class}"
      ]
      content_tag(:i, nil, class: icon_class)
    end

    def colored_good_or_bad_icon_tag(value, true_icon = 'check', false_icon = 'times', false_is_good = false)
      if false_is_good
        false_class = 'is-success'
        true_class  = 'is-danger'
      else
        false_class = 'is-danger'
        true_class  = 'is-success'
      end
      boolean_icon_tag value, true_icon, false_icon, false_class, true_class
    end

    def icon(name, classes = [], base = 'fas', pack = 'fa')
      icon_class = ["#{base} #{pack || base}-#{name}"]
      icon_class += classes.map do |c|
        if %w[2x 3x 4x fw pulse spin].include? c
          "#{pack || base}-#{c}"
        else
          c
        end
      end

      content_tag(:i, nil, class: icon_class)
    end

    def license_icon(license, classes = ['fw'])
      icon license.icon, classes, 'li', 'li'
    end

    def content_type_icon(type, options)
      type_icon = CONTENT_TYPE_ICONS.key?(type) ? CONTENT_TYPE_ICONS[type] : 'question'
      icon type_icon, options
    end

    def content_type_icon_from_entity(entity, options)
      type = entity.class.name.to_sym
      content_type_icon type, options
    end
  end
end
