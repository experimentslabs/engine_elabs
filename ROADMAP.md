# Roadmap
All the future changes will be documented here.

## [Issues]
See the [issues](https://gitlab.com/experimentslabs/website/website/issues) for a list.

## [Next] - Already done

### Added

  - Ability to export content in markdown. Enable `Elabs::exportable_markdown`
    option in the initializer to create links.
  - Notification emails for comments, reports and admin actions on user
    content.
  - User preferences to manage notification emails
  - New content type: Links. They can be tagged and have a language. Great for
    quickly sharing interesting stuff...
  - A `String` helper with helper methods like `ellipsis` and `human_model_name`
    to be used in views.
  - All content types have now "public_xxx" relations to fetch publicly
    available content.

### Changed

  - Counter caches don't handle `user`, `license` and `language` by default
    This allow creation of custom content types without all these counters.
    They are now handled with a `COUNTER_CACHES` array, which needs to contain
    all the relations.
  - Translation system was changed from Gettext to Rails defaults.
    This change breaks backward compatibility.
  - `i18n-tasks` gem was added to provide easy translation management and
    rspec tests for missing/unused translations. Use these commands:
    ```sh
    # When creating new models:
    # Generates model-related strings in app/i18n/elabs/
    bundle exec rails runner lib/runners/generate_i18n_model_attributes.rb

    # Adds missing translations to locales files (with a nice TRANSLATE_ME
    # prefix to find them)
    rake app:i18n:add-missing

    # Remove unused strings
    bundle exec i18n-tasks remove-unused

    # More actions with
    bundle exec i18n-tasks --help
    ```
  - Required gems are now defined in gemspec.

### Improved

  - Action links for members/admin indexes now have an `except` parameter
    which is used to disable some links for new custom content types.
  - Better message when JS is disabled.
  - Email views for Devise are now integrated in the engine for easier
    overriding.
  - Better messages for acts statuses.
  - Improved translations and fixed typos.
  - Content cards can be customized: if a `content_card.haml.html` file
    file exists in the content-type view directory, it will be used
    instead of the generic one.
  - "Actable" models can now specify if they log updates or not.
  - Rubocop-rails was added, which leads to several small improvements:
    - Safer rendering with all unsafe strings escaped
    - A validator for reports URLs
    - Better handling of related content destruction
  - CounterCaches are more stable with the addition of CountablePivot concern.
    This concern is to be used on pivot tables of habtm CountableEntity
    and will handle destruction of pivot data and caches updates. Check
    `albums_tag` model for an example.
  - Publication date added on content indexes
  - Make the links stand out on project pages (Home page, Documentation and Sources)

### Removed

  - Gettext_i18n_rails.

### Fixed

  - Text color for errors is now readable.
  - Filter menu is now readable on small screens.
  - RSpec command in CI was giving weird results.
  - Forms were crashing when editing content from models with no
    `SLUG_FIELD` defined.

### Known issues

  - `i18n-task` don't generate translations strings for models and models
    attributes: the `/` separator in the key seems to be an issue, but is
    needed by Rails to find the translations.

## Planned

Note that the elements in this list may change during development; it' a
general idea of what will be done next.
  - [6.0.0] - Notification emails, change the translation system and
    Links content type
  - [6.x.0] - Teams to attach user and distant people to projects
  - [7.0.0] - Projects: commits aggregation
  - [8.0.0] - Users: service status aggregation without storage for now
  - [9.0.0] - Raw content for everything

## Wonderland
  - Move HTML helpers to decorators (https://github.com/drapergem/draper)
    AND/OR use a better templating system, as [Cells](http://trailblazer.to/gems/cells/)
  - An API ?
  - Better license support (i.e.: html tags for creative commons licenses)
  - Content translations (community made)
  - Content: code snippets
  - Data aggregation for users profiles from various providers
  - Data aggregation from various VCS providers
  - Projects users
  - Sharing things across Elabs instances?
  - Support for custom fields ?
  - Teams (people with common projects)
  - ...
  - ~~Urls management (lists of URLs, favorites, sources,...)~~ (since 6.0.0)
  - ~~Export raw content (markdown, files)~~ (since 6.0.0)
  - ~~Content references (reference others work in your own work)~~
    (since 4.0.0, with shortcodes)
  - ~~Widgets (to add custom content in created content)~~ (since 4.0.0,
    with shortcodes)
  - ~~Rails engine~~ (since 2.0.0)
