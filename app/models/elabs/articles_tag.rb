module Elabs
  class ArticlesTag < ApplicationRecord
    self.table_name = 'articles_tags'

    include Elabs::Concerns::CountablePivot

    COUNTABLE_DEFINITION = %i[articles tag].freeze

    belongs_to :article
    belongs_to :tag
  end
end
