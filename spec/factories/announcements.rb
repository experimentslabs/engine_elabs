FactoryBot.define do
  factory :announcement, class: Elabs::Announcement do
    content { Faker::Lorem.paragraph }
    association :user, factory: :user_admin_active
    level { 'info' }
    target { 'global' }
  end
end
