FactoryBot.define do
  factory :license, class: Elabs::License do
    name { Faker::Lorem.word }
    url { Faker::Internet.url }
    icon { Faker::Lorem.word }
  end
end
