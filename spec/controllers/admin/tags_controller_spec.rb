require 'rails_helper'

RSpec.describe Elabs::Admin::TagsController, type: :controller do
  model              = Elabs::Tag
  invalid_attributes = { name: nil }
  valid_session      = {}

  it_behaves_like 'admin index entity', model, valid_session
  it_behaves_like 'admin update entity form', model, valid_session
  it_behaves_like 'admin update entity', model, invalid_attributes, valid_session
  it_behaves_like 'admin destroy entity', model, valid_session
end
