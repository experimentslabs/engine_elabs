# Highlight.js
Package downloaded from https://highlightjs.org/download/ with languages:

Common (all but Java, Objective-C and Perl):

  - Apache
  - Bash
  - C#
  - C++
  - CSS
  - CoffeeScript
  - Diff
  - HTML, XML
  - HTTP
  - Ini
  - JSON
  - JavaScript
  - Makefile
  - Markdown
  - Nginx
  - PHP
  - Python
  - Ruby
  - SQL
  - Shell Session


Other:

  - Dockerfile
  - ERB
  - Haml
  - Puppet
  - Scss
  - Typescript
  - Yaml



