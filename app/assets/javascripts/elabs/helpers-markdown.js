// eslint-disable-next-line no-unused-vars
function createMDPreviewer (cmInstance) {
  return function (event) {
    event.preventDefault()
    var targetElement = document.getElementById('content-description-preview')
    var data = new FormData()
    data.append('text', cmInstance.getValue())

    // eslint-disable-next-line no-undef
    ajax('POST', '/member/markdown_previewer', data, function (request) {
      targetElement.innerHTML = JSON.parse(request.responseText).html_content
    }, function () {
      targetElement.innerHTML = 'Something bad happened on our side. Sorry for that…'
    }, function () {
      targetElement.innerHTML = 'Something bad happened on our side. Sorry for that…'
    })

    openModal('content-preview-modal')
  }
}
