# Load the Rails application.
require_relative 'application'
require 'faker'
require 'factory_bot'

# Initialize the Rails application.
Rails.application.initialize!

FactoryBot.definition_file_paths.push File.join(__FILE__, '..', '..', 'spec', 'factories')
FactoryBot.find_definitions
