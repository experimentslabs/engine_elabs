# Users
user = FactoryBot.create :user_known_admin

# Languages
language = FactoryBot.create :language, iso639_1: 'fr', name: 'Français'
FactoryBot.create :language, iso639_1: 'en', name: 'English'

# Licenses
license = FactoryBot.create :license, name: 'Creative Commons by-nc-sa 4.0', url: 'https://creativecommons.org/licenses/by-nc-sa/4.0/', tldr_url: 'https://tldrlegal.com/license/creative-commons-attribution-noncommercial-sharealike-4.0-international-(cc-by-nc-sa-4.0)'
FactoryBot.create :license, name: 'GPL v2', url: 'https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html', tldr_url: 'https://tldrlegal.com/license/gnu-general-public-license-v2'

# Few tags
tag = FactoryBot.create :tag, name: 'Ruby'

default_relations = { user: user, language: language, license: license, tags: [tag] }
# Content
project = FactoryBot.create :project_published, default_relations.merge(name: 'My first project')
album   = FactoryBot.create :album_published, default_relations.merge(name: 'My first album', projects: [project])

# safe, published content
FactoryBot.create_list :article_published_and_safe, 2, default_relations.merge(projects: [project])
FactoryBot.create_list :note_published_and_safe, 2, default_relations.merge(projects: [project])
FactoryBot.create_list :upload_published_and_safe, 2, default_relations.merge(projects: [project], albums: [album])
FactoryBot.create_list :link_published_and_safe, 2, user: user, language: language, tags: [tag]

# unsafe, published content
FactoryBot.create_list :article_published, 1, default_relations.merge(projects: [project])
FactoryBot.create_list :note_published, 1, default_relations.merge(projects: [project])
FactoryBot.create_list :upload_published, 1, default_relations.merge(projects: [project], albums: [album])
FactoryBot.create_list :link_published, 2, user: user, language: language, tags: [tag]

# reports
FactoryBot.create :report

# comments
FactoryBot.create :comment, content: album

# one note with shortcodes
article = Elabs::Article.first
unsafe_article = Elabs::Article.last
unpublished_article = FactoryBot.create :article, default_relations
note = Elabs::Note.first
upload = Elabs::Upload.first
content = "## Inline shortcodes

[user:#{user.username}]
[album:#{album.slug}]
[article:#{article.slug}]
[note:#{note.slug}]
[project:#{project.slug}]
[upload:#{upload.slug}] -
[article:#{unsafe_article.slug}]
[article:this-should-not-exist]
[article:#{unpublished_article.slug}] -
[github-repo:el-cms/elabs]
[github-repo:el-cms/invalid-repo]
[gitlab-repo:https://gitlab.com/experimentslabs/engine_elabs]

## Cards

[user-card:#{user.username}]
[album-card:#{album.slug}]
[article-card:#{article.slug}]
[note-card:#{note.slug}]
[project-card:#{project.slug}]
[upload-card:#{upload.slug}]

---

[article-card:#{unsafe_article.slug}]
[article-card:this-should-not-exist]
[article-card:#{unpublished_article.slug}]

---

[github-user-card:mtancoigne]
[github-repo-card:el-cms/elabs]
[github-repo-card:el-cms/invalid-repo]
[gitlab-user-card:https://gitlab.com/mtancoigne]
[gitlab-repo-card:https://gitlab.com/experimentslabs/engine_elabs]
"
FactoryBot.create :note_published_and_safe, default_relations.merge(projects: [project], content: content)
