# Migration guide

## From 5.x to 6.0.0

First of all, read the changelog for the 5.0.0 release.

- Since the translation system has changed, add `gem 'devise-i18n'` to
  your Gemfile
- To apply the new configuration and get synced with the last changes,
  run the installation generator to have locales copied, as the
  `locale` initializer: `rails generate elabs:install`
- Update the `elabs` initializer to enable new options
- Translation system felt back to Rails defaults. You should remove:
  - `gettext_i18n_rails` gem from your Gemfile
  - `/locale`
  - `config/initializers/fast_gettext.rb`
- Apply the last migrations: `rake elabs:install:migrations` then
  `rails db:migrate`
- If you use overridden views, run `rails g elabs:views` to update the
  translation strings and new devise views.
- If you overrode Devise's mailer views, you should sync your changes
  with `app/view/elabs/devise_mailer`. You can delete your views.
- If you leave Elabs manage Devise's emails (which is the case if your
  application is base _on_ Elabs), you must edit these two values in
  `config/initializers/devise.rb`:
    - `config.mailer = 'Elabs::DeviseMailer'`
    - `config.parent_mailer = 'Elabs::ApplicationMailer'`
- Gemfile requirements for the engine have moved to the gemspec, you can
  remove dependencies from your Gemfile:
  - `active_link_to`
  - `commonmarker`
  - `devise`
  - `devise-i18n`
  - `haml-rails`
  - `kaminari `
  - `mini_magick`

If you use `gettext_i18n_rails` and want to go back to YAML files for
the rest of your application, you can follow
[this step by step guide](https://experimentslabs.com/articles/rails-switching-from-gettext_i18n_rails-to-vanilla-translations)

## From 4.x to 5.0.0

First of all, read the changelog for the 5.0.0 release.

- You can safely remove `app/assets/stylesheets` if you don't use it.
- You can safely remove `public/fonts/fa-*` as fonts are now handled by
  the assets pipeline
- You can safely remove `app/assets/images/elabs` as these images are now
  handled by the assets pipeline
- You should update the initializer for the new configuration option
  `Elabs.trap_dumb_bots`
- Layouts and styles had heavy changes, if you customized yours, you'll
  have to re-generate them and merge your work in it.

## From 3.x to 4.0.0

First of all, read the changelog for the 4.0.0 release.

Use a version control system to manage the changes and the overrides you
made in your own application.

- Add the Commonmarker gem: `gem 'commonmarker'` if you use the default
  views
- Update the base configuration (initializer, languages, ...):
  `rails g elabs:install` and accept to override everything. Then check
  changes
- Apply the last migrations: `rake elabs:install:migrations` then
  `rails db:migrate`

Don't forget to rebuild the assets...

If you have existing content you need to redirect you can use a runner
like this one:

```rb
# Example for Apache
# Uncomment if you need to enable RewriteEngine (mod_rewrite is required)
puts "# RewriteEngine  on"

[
    { model: Elabs::Language, subs: %w[albums articles notes projects uploads] },
    { model: Elabs::License, subs: %w[albums articles notes projects uploads] },
    { model: Elabs::Tag, subs: %w[albums articles notes projects uploads] },
    { model: Elabs::User, subs: %w[albums articles notes projects uploads] },
    { model: Elabs::Album, subs: [] },
    { model: Elabs::Article, subs: [] },
    { model: Elabs::Note, subs: [] },
    { model: Elabs::Project, subs: %w[albums articles notes uploads] },
    { model: Elabs::Upload, subs: [] },
].each do |config|
  slug_field = config[:model]::SLUG_FIELD
  endpoint   = config[:model].to_s.demodulize.tableize
  puts '# Entities redirection'
  config[:model].all.each do |record|
    # Entity
    puts "Redirect 301 /#{endpoint}/#{record.id} /#{endpoint}/#{record.send(slug_field)}"

    # Nested resources
    config[:subs].each do |sub|
      puts "Redirect 301 /#{endpoint}/#{record.id}/#{sub} /#{endpoint}/#{record.send(slug_field)}/#{sub}"
    end
  end
end
```

## From 2.x to 3.0.0

First of all, read the changelog for the 3.0.0 release

- Add the "Rails-i18n" gem for some core translations: `gem 'rails-i18n'`
- Update the translations: `rails g elabs:install` and accept to override
  the initializer, and everything in `locale` folder
- Apply the last migrations: `rake elabs:install:migrations` then
  `rails db:migrate`

If you have overridden things, you will need to check them manually. You
can use the various generators to re-copy the elabs views/assets,... and
and compare them with your version (hint: you should use a version
control system to avoid messing up things).

## From 1.0.1 to 2.0.0
No one ever used 1.0.0 in production, so there is no guide for it :)
