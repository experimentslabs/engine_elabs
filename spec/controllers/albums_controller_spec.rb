require 'rails_helper'

RSpec.describe Elabs::AlbumsController, type: :controller do
  model               = Elabs::Album
  valid_nested_routes = %w[user license tag language project]
  valid_session       = {}

  it_behaves_like 'public index content', model, valid_session, valid_nested_routes
  it_behaves_like 'public show content', model, valid_session
end
