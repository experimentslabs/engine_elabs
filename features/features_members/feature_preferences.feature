Feature: preferences
  As a member
  In order to have a good experience
  I want to be able to save my preferences regarding the website

  Background:
    Given I am a member with email "john.do@recovery.com"
    And a website with an "en" language for content in "English"
    And a website with 1 unsafe, published element of every type created by "john.do@recovery.com"

  Scenario: Change the NSFW preference
    Given I set the NSFW preferences to display NSFW content
    When I login again as "john.do@recovery.com" with password "password"
    Then I should see "Hide NSFW"

  Scenario: Change the locale preference
    Given I set the locale preferences to "Français"
    When I login again as "john.do@recovery.com" with password "password"
    Then I should see "Déconnexion"
