class AddReasonToActs < ActiveRecord::Migration[5.2]
  def change
    add_column :acts, :reason, :string, default: nil, null: true
  end
end
