builder.item do
  builder.title rss_title(project, 'name')
  builder.description "#{project.short_description}\n\n#{project.description}"
  builder.pubDate project.published_at.to_s(:rfc822)
  builder.link project_url(project)
  builder.guid project_url(project)
end
