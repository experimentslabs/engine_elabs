RSpec.shared_examples 'admin lock content' do |model, valid_session|
  routes { Elabs::Engine.routes }
  let(:singular_entity) { model.name.demodulize.downcase }
  let(:ident_field) { model.const_defined?(:SLUG_FIELD) ? model::SLUG_FIELD : :id }

  include_context 'with authenticated admin'

  describe 'PUT #lock' do
    it "locks the requested #{model.name.demodulize.downcase}" do
      entity        = FactoryBot.create(singular_entity)
      entity.locked = true
      put :toggle_lock, params: { "#{singular_entity}_#{ident_field}" => entity.to_param }, session: valid_session
      entity.reload
      expect(entity.locked?).to be_truthy
    end

    it "redirects to the #{model.name.demodulize.downcase.pluralize} list" do
      entity        = FactoryBot.create(singular_entity)
      entity.locked = true
      put :toggle_lock, params: { "#{singular_entity}_#{ident_field}" => entity.to_param }, session: valid_session
      expect(response).to redirect_to("admin_#{singular_entity.pluralize}".to_sym)
    end

    it 'notifies the content author if commenter is not author' do
      entity        = FactoryBot.create("#{singular_entity}_published")
      entity.locked = true
      expect do
        put :toggle_lock, params: { "#{singular_entity}_#{ident_field}" => entity.to_param }, session: valid_session
      end.to change(Elabs::Notification, :count).by(1)
      expect(Elabs::Notification.last.event).to eq('lock')
    end
  end
end

RSpec.shared_examples 'admin destroy content' do |model, valid_session = {}|
  routes { Elabs::Engine.routes }
  let(:singular_entity) { model.name.demodulize.downcase }
  let(:ident_field) { model.const_defined?(:SLUG_FIELD) ? model::SLUG_FIELD : :id }

  include_context 'with authenticated admin'

  it 'notifies the content author if commenter is not author' do
    entity        = FactoryBot.create(singular_entity)
    entity.locked = true
    expect do
      delete :destroy, params: { ident_field => entity.to_param }, session: valid_session
    end.to change(Elabs::Notification, :count).by(1)
    expect(Elabs::Notification.last.event).to eq('delete')
  end
end
