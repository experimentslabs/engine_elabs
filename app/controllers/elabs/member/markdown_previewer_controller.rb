module Elabs
  module Member
    class MarkdownPreviewerController < ElabsApplicationController
      self.allow_forgery_protection = false

      def preview
        render locals: { text: params['text'] }
      end
    end
  end
end
