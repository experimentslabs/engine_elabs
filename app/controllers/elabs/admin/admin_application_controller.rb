module Elabs
  module Admin
    class AdminApplicationController < ElabsApplicationController
      DEFAULT_ORDER_FIELD = 'id'.freeze
      MAX_ITEMS_PER_PAGE  = Elabs.max_admin_items_per_page

      before_action :authenticate_admin!

      layout 'elabs/layouts/application_admin'

      private

      def authenticate_admin!
        authenticate_user!
        redirect_to user_url(current_user), status: :unauthorized unless current_user.admin?
      end
    end
  end
end
