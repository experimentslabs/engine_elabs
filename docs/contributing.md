# How to contribute ?
New ideas and contributions are more than welcome.

Read the [Code of conduct](../CODE_OF_CONDUCT.md) if you're unsure of
how you're doing things.

The issue tracker is on [gitlab](https://gitlab.com/experimentslabs/engine_elabs/issues). Feel free to open issues here.

## Translations

Rake tasks:
```
rake app:i18n:add-missing          # Add missing translations
rake app:i18n:add-model-attributes # Generate a file with models attributes
```

## Git workflow and code submission
  - Branches `master` and `develop` are _fast-forward_ only.
  - Make all the MR against `develop` or a release branch if it exists.
  - Every MR should pass the test suite.

## Notes

- Email previews should be stored in `spec/dummy/test/previews/elabs/`
- Email previews uses data from the database to avoid creating new entries.
  You should have ran the seeds during development to have them
  (`rails db:seed`)
- If you want to use a system like [MailHog](https://github.com/mailhog/MailHog)
  to preview the fully generated emails, you can temporarily change
  `spec/dummy/environments/development.rb`:
  ```rb
  config.action_mailer.perform_deliveries = true
  config.action_mailer.smtp_settings      = {
      address: 'localhost',
      port:    1025 # MailHog port
  }
  ```

## Testing
This project uses various tools to enforce code consistency and to run tests:

  - [Rubocop](http://batsov.com/rubocop/) to check the code style. Run
    `bundle exec rubocop` or `bundle exec rubocop -a` to fix some errors
    automatically.
  - [Brakeman](https://brakemanscanner.org/), to check the code against
    known vulnerabilities and possible security issues. You can launch
    it locally with `bundle exec brakeman`.
  - [RSpec](http://rspec.org/) for unit test. You can launch it locally
    with `bundle exec rspec`.
  - [Cucumber](https://cucumber.io/) for integration tests. Run
    `bundle exec cucumber` locally to test the code.
  - [Eslint](https://eslint.org/) for JS code style. Launch
    `yarn run lint:js` or `yarn run lint:js-fix` to check locally. It's
    not perfect, but it helps.
  - [StyleLint](http://stylelint.io/) for SCSS code style. Run
    `yarn run lint:sass` or `yarn run lint:sass-fix` locally.
  - [Haml-lint](https://github.com/brigade/haml-lint) for views
    consistency. Run `bundle exec haml-lint`.
