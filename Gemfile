source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

# Declare your gem's dependencies in elabs.gemspec.
# Bundler will treat runtime dependencies like base dependencies, and
# development dependencies will be added by default to the :development group.
gemspec

# Declare any dependencies that are still in development here instead of in
# your gemspec. These might include edge Rails or gems from your path or
# Git. Remember to move these dependencies to your gemspec before releasing
# your gem to rubygems.org.

group :development, :test do
  # -------------------------------------------------------
  # Gems needed for the dummy application
  # -------------------------------------------------------
  gem 'jbuilder', '~> 2.5'
  # See https://github.com/rails/execjs#readme for more supported runtimes
  gem 'mini_racer', platforms: :ruby
  # Use ActiveStorage variant
  # gem 'mini_magick', '~> 4.8'
  # DB
  gem 'pg'
  # Translations
  gem 'i18n-tasks'
  gem 'rails-i18n'
  # Style
  gem 'sass-rails', '~> 5.0'

  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: %i[mri mingw x64_mingw]
  # Windows does not include zoneinfo files, so bundle the tzinfo-data gem
  gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]
end

group :development do
  # Access an interactive console on exception pages or by calling 'console' anywhere in the code.
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'web-console', '>= 3.3.0'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

group :test do
  # Adds support for Capybara system testing and selenium driver
  gem 'capybara', '>= 2.15', '< 4.0'
  gem 'capybara-screenshot'
  gem 'selenium-webdriver'
  # Easy installation and use of chromedriver/geckodriver to run system tests with Chrome
  gem 'webdrivers'
  # Cucumber
  gem 'cucumber-rails', require: false
  gem 'database_cleaner'
  # Factories
  gem 'factory_bot_rails'
  gem 'faker'
  # Linters
  gem 'haml_lint', require: false
  gem 'rubocop'
  gem 'rubocop-faker'
  gem 'rubocop-performance'
  gem 'rubocop-rails'
  # Rspec
  gem 'rails-controller-testing'
  gem 'rspec-rails'
  gem 'rspec-rails-api'
  gem 'ruby_parser', require: false
  gem 'simplecov', require: false
  # Security scanner
  gem 'brakeman'
end
