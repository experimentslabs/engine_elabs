// eslint-disable-next-line no-unused-vars
function initializeCodeMirrorIn (element) {
  return CodeMirror.fromTextArea(element, {
    lineNumbers: true,
    mode: {
      name: 'gfm',
      tokenTypeOverrides: {
        emoji: 'emoji'
      }
    },
    theme: 'base16-light'
  })
}

// eslint-disable-next-line no-unused-vars
function limitCMToLength (cm, change) {
  var maxLength = cm.getOption('maxLength')
  if (maxLength && change.update) {
    var str = change.text.join('\n')
    var delta = str.length - (cm.indexFromPos(change.to) - cm.indexFromPos(change.from))
    if (delta <= 0) { return true }
    delta = cm.getValue().length + delta - maxLength
    if (delta > 0) {
      str = str.substr(0, str.length - delta)
      change.update(change.from, change.to, str.split('\n'))
    }
  }
  return true
}
