module Elabs
  module Concerns
    # This concern is meant to be included in controllers extending
    # other plugins/engines controllers (i.e.: Devise)
    # This should not be used on Elabs controllers, as everything here is declared
    # in ElabsApplicationController.
    module ElabsController
      extend ActiveSupport::Concern

      included do
        helper Elabs::ApplicationHelper
        helper Elabs::IconsHelper
        helper Elabs::ContentFiltersHelper
        helper Elabs::NotificationHelper
        helper Elabs::ContentRendererHelper
        helper Elabs::ShortcodesHelper

        layout 'elabs/layouts/application'
      end
    end
  end
end
