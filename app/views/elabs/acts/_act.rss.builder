if act.event == 'publish'
  case act.content_type
  when 'Elabs::Album'
    render 'elabs/albums/album', album: act.content, builder: builder
  when 'Elabs::Article'
    render 'elabs/articles/article', article: act.content, builder: builder
  when 'Elabs::Note'
    render 'elabs/notes/note', note: act.content, builder: builder
  when 'Elabs::Project'
    render 'elabs/projects/project', project: act.content, builder: builder
  when 'Elabs::Upload'
    render 'elabs/uploads/upload', upload: act.content, builder: builder
  when 'Elabs::Link'
    render 'elabs/links/link', link: act.content, builder: builder
  end
else
  builder.item do
    builder.title act_notice_string(act, false)
    builder.description act_notice_string(act)
    builder.pubDate act.created_at.to_s(:rfc822)
    builder.link url_for act.content
    builder.guid url_for act.content
  end
end
