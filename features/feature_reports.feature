Feature: Report content
  As a maintainer
  In order to keep the site clean
  I want the visitors to be able to report issues or content they find inappropriate

  Scenario: Report a page as an user
    Given I am not logged in
    When I visit the home page
    And I submit a report as an user
    Then A message should tell me the report is saved.

  Scenario: Report a page as a member
    Given I am a member
    When I visit the home page
    And I submit a report as a member
    Then A message should tell me the report is saved.

  Scenario: Admin can list the reports
    Given I am an administrator
    And a website with 3 reports
    When I visit the admin reports page
    Then I should see 3 reports in the list

  Scenario: Simple bot report
    Given I am not logged in
    When I visit the home page
    And I submit a report as a bot
    Then I should see a success message for bots
