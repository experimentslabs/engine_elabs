# frozen_string_literal: true

require 'rails/generators/base'

module Elabs
  module Generators
    class ImagesGenerator < Rails::Generators::Base
      source_root File.expand_path('../../..', __dir__)

      def set_source_paths
        @source_paths = [
          File.expand_path('../templates', __dir__),
          File.expand_path('../../..', __dir__)
        ]
      end

      desc 'Copies placeholder images'

      def copy_images
        directory 'app/assets/images/elabs/'
      end
    end
  end
end
