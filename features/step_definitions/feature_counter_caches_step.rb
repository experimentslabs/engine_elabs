Then(/^counters for ((?:[a-z_]*(?:, )?)+) should be showing (\d+)$/) do |counters, amount|
  counters.split(/, */).each do |counter|
    expect(page).to have_content("#{counter.capitalize}: #{amount}")
  end
end
