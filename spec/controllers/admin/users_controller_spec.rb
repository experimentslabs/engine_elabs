require 'rails_helper'
RSpec.describe Elabs::Admin::UsersController, type: :controller do
  model              = Elabs::User
  valid_session      = {}

  it_behaves_like 'admin index entity', model, valid_session
  it_behaves_like 'admin show entity', model, valid_session
  it_behaves_like 'admin destroy entity', model, valid_session
end
