Feature: Generic content management
  As a maintainer
  In order to protect visitors
  I want to be able to delete and lock content posted by members

  Background:
    Given I am an administrator
    And a website with a "fr" language for content in "Français"
    And a website with a "VueJS" tag
    And a website with a "GPL 3.0" license
    And a website with one sfw, published album titled "My first album"
    And a website with one sfw, published article titled "My first article"
    And a website with one sfw, published link titled "My first link"
    And a website with one sfw, published note containing "My first note"
    And a website with one sfw, published project titled "My first project"
    And a website with one sfw, published upload titled "My first upload"

  Scenario Outline: Admin display content
    Given I visit the admin <plural_type> page
    Then I should see a list of <plural_type>
    And I should see admin generic information for every "<singular_type>" element

    Examples:
      | singular_type | plural_type |
      | album         | albums      |
      | article       | articles    |
      | link          | links       |
      | note          | notes       |
      | project       | projects    |
      | upload        | uploads     |

  Scenario Outline: Delete content
    When I delete the <singular_type> containing "<title>" via the admin zone
    Then I should not see the <singular_type> containing "<title>" in the admin zone

    Examples:
      | title            | singular_type |
      | My first album   | album         |
      | My first article | article       |
      | My first link    | link          |
      | My first note    | note          |
      | My first project | project       |
      | My first upload  | upload        |

  Scenario Outline: Admin lock content
    Given I lock the "<title>" <singular_type>
    Then The "View online" link should be disabled for the "<title>" <singular_type>
    When I visit the public <plural_type> page
    Then I should not see the "<title>" in the public <singular_type> list
    And I should not be able to see the public "<title>" <singular_type> manually

    Examples:
      | title            | singular_type | plural_type |
      | My first album   | album         | albums      |
      | My first article | article       | articles    |
      | My first note    | note          | notes       |
      | My first project | project       | projects    |
      | My first upload  | upload        | uploads     |
