Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  # Home page
  root 'elabs/acts#index'

  # Elabs routes
  mount Elabs::Engine, at: '/'
end
