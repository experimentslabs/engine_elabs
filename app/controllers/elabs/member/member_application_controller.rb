module Elabs
  module Member
    class MemberApplicationController < ElabsApplicationController
      DEFAULT_ORDER_FIELD = 'id'.freeze
      MAX_ITEMS_PER_PAGE  = Elabs.max_members_items_per_page

      before_action :authenticate_user!

      layout 'elabs/layouts/application_member'
    end
  end
end
