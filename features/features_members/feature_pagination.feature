Feature: Pagination for users
  As a website maintainer,
  In order to preserve the bandwidth
  I want to paginate index pages which can have a lot of content

  Background:
    Given I am a member with email "bobby@example.com"
    And a website with 20 safe, published elements of every type created by "bobby@example.com"

  # We want all the licenses and tags
  Scenario Outline: Check maximum elements on a public page
    When I visit the member <plural_type> page
    Then I should see <amount> <plural_type>

    Examples:
      | plural_type | amount |
      | albums      | 15     |
      | articles    | 15     |
      | links       | 15     |
      | notes       | 15     |
      | projects    | 15     |
      | uploads     | 15     |
