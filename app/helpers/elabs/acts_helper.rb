module Elabs
  module ActsHelper
    def act_action(action)
      actions = {
        create:    t('elabs.acts_helper.act_action.created'),
        update:    t('elabs.acts_helper.act_action.updated'),
        lock:      t('elabs.acts_helper.act_action.locked'),
        unlock:    t('elabs.acts_helper.act_action.unlocked'),
        publish:   t('elabs.acts_helper.act_action.published'),
        unpublish: t('elabs.acts_helper.act_action.removed_from_publication')
      }.freeze
      actions[action.to_sym] || "[missing: #{action}]"
    end

    def act_notice_string(act, include_link = true)
      type   = act.content_type.demodulize.underscore
      title  = if %w[unpublish destroy lock].include?(act.event) || !include_link
                 html_escape act.content.title_to_display
               else
                 link_to(act.content.title_to_display, act.content)
               end

      # rubocop:disable Rails/OutputSafety
      # "title" is the only threat here and it is escaped.
      t("elabs.acts_helper.act_notice_string.#{type}_#{act.event}_event", name: title).html_safe
      # rubocop:enable Rails/OutputSafety
    end
  end
end
