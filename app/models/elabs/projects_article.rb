module Elabs
  class ProjectsArticle < ApplicationRecord
    self.table_name = 'projects_articles'

    include Elabs::Concerns::CountablePivot

    COUNTABLE_DEFINITION = %i[articles project].freeze

    belongs_to :project
    belongs_to :article
  end
end
