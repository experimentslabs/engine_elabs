# Shortcodes guide

Reference file: `app/helpers/shortcodes_helper.rb`

General behavior:
 - If a card shortcode is made to some site content, the card content
   won't be visible in indexes to limit the requests
 - If a card shortcode points to the content in which it is created, a
   warning is displayed instead, to avoid infinite loops
 - If a card shortcode contains shortcodes too, these shortcodes won't
   be displayed to limit the requests and to keep the page clean

## Inline shortcodes

| Shortcode                       | Example                                                         | Description                                                  |
|---------------------------------|-----------------------------------------------------------------|--------------------------------------------------------------|
| `[album:<album-slug>]`          | `[album:my-first-album]`                                        | Link with album icon and name                                |
| `[article:<article-slug>]`      | `[article:my-first-article]`                                    | Link with article icon and title                             |
| `[note:<note-slug>]`            | `[note:2018-11-23-18-57-02-3]`                                  | Link with note icon and id                                   |
| `[project:<project-slug>]`      | `[project:my-first-project]`                                    | Link with project icon and name                              |
| `[upload:<upload-slug>]`        | `[upload:my-first-upload]`                                      | Link with upload icon and title                              |
| `[user:<username>]`             | `[user:jimmy]`                                                  | Link with avatar if any                                      |
| `[github-repo:<username/repo>]` | `[github-repo:el-cms/elabs]`                                    | Github icon, link to user, link to repo with a `/` separator |
| `[gitlab-repo:<link>]`          | `[gitlab-repo:https://gitlab.com/experimentslabs/engine_elabs]` | Gitlab icon, link to user, link to repo with a `/` separator |

Behavior:
 - for site-content, if content is unpublished/inexistant, a warning is displayed instead
 - for site-content, if content is NSFW, a warning is displayed, depending on current NSFW preference
 - for Github/Gitlab links, if the targeted element does not exist, the links are created anyway.

## Cards

| Shortcode                           | Example                                                              | Description                                |
|-------------------------------------|----------------------------------------------------------------------|--------------------------------------------|
| `[album-card:<album-slug>]`         | `[album-card:my-first-album]`                                        | Simple card as the ones visible in indexes |
| `[article-card:<article-slug>]`     | `[article-card:my-first-article]`                                    | Simple card as the ones visible in indexes |
| `[note-card:<note-slug>]`           | `[note-card:2018-11-23-18-57-02-3]`                                  | Simple card as the ones visible in indexes |
| `[project-card:<project-slug>]`     | `[project-card:my-first-project]`                                    | Simple card as the ones visible in indexes |
| `[upload-card:<upload-slug>]`       | `[upload-card:my-first-upload]`                                      | Simple card as the ones visible in indexes |
| `[user-card:<username>]`            | `[user-card:jimmy]`                                                  | Simple card as the ones visible in indexes |
| `[github-repo-card:<username/repo>` | `[github-repo-card:el-cms/elabs]`                                    | Card with base information and description |
| `[github-user-card:<username>`      | `[github-user-card:mtancoigne]`                                      | Card with base information                 |
| `[gitlab-repo-card:<link>`          | `[gitlab-repo-card:https://gitlab.com/experimentslabs/engine_elabs]` | Card with base information and description |
| `[gitlab-user-card:<link>`          | `[gitlab-user-card:https://gitlab.com/mtancoigne]`                   | Card with base information                 |
| `[gitlab-group-card:<link>`         | `[gitlab-group-card:https://gitlab.com/experimentslabs]`             | Card with base information                 |

Behavior:
 - for site-content, if content is unpublished/inexistant, a warning is displayed instead
 - for site-content, if content is NSFW, a warning is displayed, depending on current NSFW preference
 - for Github/Gitlab links, if the targeted element does not exist, an error is displayed.
 - for Github cards, the visitor may reach a limit of cards displayed, as we don't send any developer key to github.
