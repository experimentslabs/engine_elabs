Feature: Content tag
  As a maintainer
  In order to allow creators to write in their native tag
  I want to be able to manage tags attachable to contents

  Background:
    Given I am an administrator
    And a website with a "C++" tag

  Scenario: Admin display tags
    Given I visit the admin tags page
    Then I should see a list of tags

  Scenario: Admin update tags
    Given I change a tag to "C#"
    Then The tag should now have the "C#" name

  Scenario: Admin delete tags
    When I delete the "C++" tag
    Then I should see that the "C++" tag is not present in the list
