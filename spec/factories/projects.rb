FactoryBot.define do
  factory :project, class: Elabs::Project do
    name { Faker::Lorem.sentence }
    short_description { Faker::Lorem.sentence }
    description { Faker::Lorem.paragraph }
    license
    language
    user

    factory :project_published do
      published { true }
      published_at { Time.zone.now }

      factory :project_published_and_safe do
        sfw { true }
      end
    end
  end
end
