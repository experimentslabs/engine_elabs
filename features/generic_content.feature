Feature: Content management
  As a content creator,
  In order to provide various content
  I want to show the content I created

  Background:
    Given I am not logged in
    And a website with 2 safe, published elements of every type

  Scenario Outline: Display a content list
    When I visit the <plural_type> page
    Then I should see <amount> <singular_type>
    Then I should see public generic information for every "<singular_type>" element

    Examples:
      | singular_type | plural_type | amount |
      | album         | albums      | 2      |
      | article       | articles    | 2      |
      | link          | links       | 2      |
      | note          | notes       | 2      |
      | project       | projects    | 2      |
      | upload        | uploads     | 2      |

  Scenario Outline: View a content item
    Given I visit the "first" public <singular_type> page
    Then I should see the "<singular_type>" details

    Examples:
      | singular_type |
      | album         |
      | article       |
      | note          |
      | project       |
      | upload        |

  Scenario Outline: View a related lists of items
    Given I visit the "first" public <singular_type> page
    Then I should see the first "<related_types>" related to "<singular_type>"


    Examples:
      | singular_type | related_types                                     |
      | language      | albums, articles, notes, projects, uploads, links |
      | license       | albums, articles, notes, projects, uploads        |
      | project       | albums, articles, notes, uploads                  |
      | tag           | albums, articles, notes, projects, uploads, links |
      | user          | albums, articles, notes, projects, uploads, links |

  Scenario: Don't show links to related unpublished projects
    Given I am a member with email "bob@example.com"
    Given a website with one sfw, unpublished project, "The hidden project" which I created
    And a website with one sfw, unpublished album, "The hidden album" which I created
    And a website with one sfw, published upload, "The great upload" which I created, linked to project "The hidden project" and album "The hidden album"
    When I visit the "The great upload" public uploads page
    And I should not see "The hidden project" and "The hidden album"
