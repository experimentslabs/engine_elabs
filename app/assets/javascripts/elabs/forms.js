// eslint-disable-next-line no-unused-vars
function toggleUpdateMessageField (event, element) {
  var checked = event.target.checked
  if (checked) {
    document.getElementById(element).setAttribute('disabled', 'disabled')
  } else {
    document.getElementById(element).removeAttribute('disabled')
  }
}
