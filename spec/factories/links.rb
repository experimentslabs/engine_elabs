FactoryBot.define do
  factory :link, class: Elabs::Link do
    title { Faker::Lorem.sentence }
    url { Faker::Internet.url }
    user
    language

    factory :link_published do
      published { true }
      published_at { Time.zone.now }

      factory :link_published_and_safe do
        sfw { true }
      end
    end
  end
end
