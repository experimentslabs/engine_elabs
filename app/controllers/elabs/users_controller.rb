module Elabs
  class UsersController < ElabsPublicController
    ALLOWED_ORDER_FROM = %w[username created_at].freeze
    DEFAULT_ORDER      = { username: :asc }.freeze

    before_action :set_user, only: %i[show]

    # GET /users
    # GET /users.json
    def index
      @users = scope_request User.page(params[:page]).per(self.class::MAX_ITEMS_PER_PAGE)
    end

    # GET /users/1
    # GET /users/1.json
    def show; end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find_by!(username: params[:username])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.fetch(:user, {})
    end
  end
end
