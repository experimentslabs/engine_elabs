// eslint-disable-next-line no-unused-vars
function openModal (e, id) {
  if (!id) {
    id = e
  } else {
    e.preventDefault()
  }
  addClass(document.getElementById(id), 'modal--visible')
}

// eslint-disable-next-line no-unused-vars
function closeModal (e, id) {
  if (!id) {
    id = e
  } else {
    e.preventDefault()
  }
  removeClass(document.getElementById(id), 'modal--visible')
}
