# frozen_string_literal: true

require 'rails/generators/base'

module Elabs
  module Generators
    class ViewsGenerator < Rails::Generators::Base
      source_root File.expand_path('../../..', __dir__)

      def set_source_paths
        @source_paths = [
          File.expand_path('../templates', __dir__),
          File.expand_path('../../..', __dir__)
        ]
      end

      desc 'Makes a copy of elabs views and assets in your app'

      def copy_views
        directory 'app/views/elabs'
      end
    end
  end
end
