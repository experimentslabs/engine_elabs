# frozen_string_literal: true

require 'rails/generators/base'

module Elabs
  module Generators
    class AssetsGenerator < Rails::Generators::Base
      source_root File.expand_path('../../..', __dir__)

      def set_source_paths
        @source_paths = [
          File.expand_path('../templates', __dir__),
          File.expand_path('../../..', __dir__)
        ]
      end

      desc 'Copies assets needed by Elabs for you to customize'

      def copy_js
        directory 'app/assets/javascripts/elabs'
      end

      def copy_css
        directory 'app/assets/stylesheets/elabs'
      end

      def copy_js_vendors
        directory 'lib/assets/javascripts'
      end

      def copy_css_vendors
        directory 'lib/assets/stylesheets'
      end
    end
  end
end
