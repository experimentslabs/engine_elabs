// eslint-disable-next-line no-unused-vars
function hideAnnounce (id) {
  var announces = localStorage.getItem('hiddenAnnounces')

  if (announces) {
    announces = JSON.parse(announces)
  } else {
    announces = []
  }

  announces.push(id)

  localStorage.setItem('hiddenAnnounces', JSON.stringify(announces))
  addClass(document.getElementById('announce-' + id), 'hidden')
}

function hideAnnounces () {
  var announces = localStorage.getItem('hiddenAnnounces')

  if (!announces) return

  announces = JSON.parse(announces)

  for (var a in announces) {
    var element = document.getElementById('announce-' + announces[a])
    if (element) addClass(element, 'hidden')
  }
}

// eslint-disable-next-line no-undef
documentReady(function () {
  hideAnnounces()
})
