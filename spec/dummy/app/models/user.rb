class User < Elabs::User
  # Include default devise modules. Others available are:
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :confirmable,
         :recoverable, :rememberable, :validatable
  devise :registerable if Elabs.users_can_register
end
