module Elabs
  class ProjectsUpload < ApplicationRecord
    self.table_name = 'projects_uploads'

    include Elabs::Concerns::CountablePivot

    COUNTABLE_DEFINITION = %i[uploads project].freeze

    belongs_to :project
    belongs_to :upload
  end
end
