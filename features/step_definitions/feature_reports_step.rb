Given('a website with {int} reports') do |_int|
  FactoryBot.create_list :report, 3
end

When(/^I submit a report as a(?:n?) (user|member|bot)$/) do |type|
  within('#new_report') do
    case type
    when 'user'
      fill_in 'Name', with: 'Bob'
      fill_in 'Email', with: 'bob@example.com'
    when 'bot'
      fill_in 'Body', with: 'Some bot propaganda'
    else
      expect(page).to have_content('Report will be posted as')
    end

    fill_in 'Reason', with: 'Some content'

    click_on 'Save'
  end
end

Then('A message should tell me the report is saved.') do
  expect(page).to have_content 'Report was successfully created.'
end

Then('I should see {int} reports in the list') do |amount|
  expect(all('.reports-list .report').count).to eq(amount)
end
