class RemoveLicenseTimestamps < ActiveRecord::Migration[5.2]
  def change
    remove_column :licenses, :created_at # rubocop:disable Rails/ReversibleMigration
    remove_column :licenses, :updated_at # rubocop:disable Rails/ReversibleMigration
  end
end
