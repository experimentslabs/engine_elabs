module Elabs
  class DeviseMailer < Devise::Mailer
    def confirmation_instructions(record, token, opts = {})
      mail = super
      # your custom logic
      mail.subject = @resource.try(:confirmed?) ? t('.subject_change', site_name: Elabs.site_name) : t('.subject_new', site_name: Elabs.site_name)
      mail
    end

    def reset_password_instructions(record, token, opts = {})
      mail = super
      # your custom logic
      mail.subject = t('.subject', site_name: Elabs.site_name)
      mail
    end

    def unlock_instructions(record, token, opts = {})
      mail = super
      # your custom logic
      mail.subject = t('.subject', site_name: Elabs.site_name)
      mail
    end

    def email_changed(record, opts = {})
      mail = super
      # your custom logic
      mail.subject = t('.subject', site_name: Elabs.site_name)
      mail
    end

    def password_change(record, opts = {})
      mail = super
      # your custom logic
      mail.subject = t('.subject', site_name: Elabs.site_name)
      mail
    end
  end
end
