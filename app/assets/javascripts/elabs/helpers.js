// eslint-disable-next-line no-unused-vars
function removeClass (element, className) {
  if (element.classList) {
    element.classList.remove(className)
  } else {
    element.className = element.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ')
  }
}

// eslint-disable-next-line no-unused-vars
function addClass (element, className) {
  if (element.classList) {
    element.classList.add(className)
  } else {
    element.className += ' ' + className
  }
}

// eslint-disable-next-line no-unused-vars
function hasClass (element, className) {
  if (element.classList) {
    return element.classList.contains(className)
  }
  return (' ' + element.className + ' ').indexOf(' ' + className + ' ') > -1
}

// eslint-disable-next-line no-unused-vars
function toggleClass (element, className) {
  if (hasClass(element, className)) {
    removeClass(element, className)
  } else {
    addClass(element, className)
  }
}

// eslint-disable-next-line no-unused-vars
function documentReady (fn) {
  if (document.attachEvent ? document.readyState === 'complete' : document.readyState !== 'loading') {
    fn()
  } else {
    document.addEventListener('DOMContentLoaded', fn)
  }
}

// eslint-disable-next-line no-unused-vars
function ajax (method, url, data, success, failure, error) {
  var request = new XMLHttpRequest()
  request.open(method, url, true)
  request.setRequestHeader('Accept', 'application/json')

  request.onload = function () {
    if (request.status >= 200 && request.status < 400) {
      if (typeof success === 'function') success(request)
    } else {
      if (typeof failure === 'function') failure(request)
    }
  }
  request.onerror = function () {
    if (typeof error === 'function') error(request)
  }
  request.send(data)
}
