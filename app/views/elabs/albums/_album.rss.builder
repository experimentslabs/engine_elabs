builder.item do
  builder.title rss_title(album, 'name')
  builder.description album.description
  builder.pubDate album.published_at.to_s(:rfc822)
  builder.link album_url(album)
  builder.guid album_url(album)
end
