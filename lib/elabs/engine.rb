require 'devise'
require 'haml'
require 'kaminari'
require 'commonmarker'
require 'active_link_to'

module Elabs
  class Engine < ::Rails::Engine
    isolate_namespace Elabs
  end
end
