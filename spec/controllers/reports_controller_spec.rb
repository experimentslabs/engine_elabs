require 'rails_helper'

RSpec.describe Elabs::ReportsController, type: :controller do
  routes { Elabs::Engine.routes }

  let(:valid_attributes) { FactoryBot.build(:report).attributes.symbolize_keys }
  let(:invalid_attributes) { { reason: nil } }
  let(:valid_session) { {} }

  describe 'POST #create' do
    context 'with valid params' do
      it 'creates a new Report' do
        request.env['HTTP_REFERER'] = '/'
        expect do
          post :create, params: { report: valid_attributes }, session: valid_session
        end.to change(Elabs::Report, :count).by(1)
      end

      it 'notifies the admins' do
        count = Elabs::User.admins.count
        request.env['HTTP_REFERER'] = '/'
        expect do
          post :create, params: { report: valid_attributes }, session: valid_session
        end.to change(Elabs::Notification, :count).by(count)
      end
    end

    context 'with invalid params' do
      it "returns a success response (i.e. to display the 'new' template)" do
        request.env['HTTP_REFERER'] = '/'
        post :create, params: { report: invalid_attributes }, session: valid_session
        expect(response).to have_http_status(302)
      end
    end
  end
end
