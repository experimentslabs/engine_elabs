Elabs::Engine.routes.draw do
  # Devise routes: use Elabs custom controllers (adds support for username and preferences)
  devise_for :users, path: 'auth', controllers: {
    confirmations: 'elabs/auth/confirmations',
    passwords:     'elabs/auth/passwords',
    registrations: 'elabs/auth/registrations',
    sessions:      'elabs/auth/sessions',
    unlocks:       'elabs/auth/unlocks'
  }
  # Resources
  resources :albums,    only: %i[index show], param: :slug do
    resources :uploads, only: %i[index]
    post 'comment', to: 'albums#create_comment', as: 'create_comment', param: :slug
  end
  resources :articles,  only: %i[index show], param: :slug do
    post 'comment', to: 'articles#create_comment', as: 'create_comment', param: :slug
  end
  resources :notes, only: %i[index show], param: :slug do
    post 'comment', to: 'notes#create_comment', as: 'create_comment', param: :slug
  end
  resources :projects, only: %i[index show], param: :slug do
    resources :articles, only: %i[index]
    resources :albums,   only: %i[index]
    resources :notes,    only: %i[index]
    resources :uploads,  only: %i[index]
    post 'comment', to: 'projects#create_comment', as: 'create_comment', param: :slug
  end
  resources :uploads, only: %i[index show], param: :slug do
    post 'comment', to: 'uploads#create_comment', as: 'create_comment', param: :slug
  end
  resources :links, only: %i[index]

  # Classifiers
  resources :languages, only: %i[index show], param: :iso639_1 do
    resources :albums,   only: %i[index]
    resources :articles, only: %i[index]
    resources :links,    only: %i[index]
    resources :notes,    only: %i[index]
    resources :projects, only: %i[index]
    resources :uploads,  only: %i[index]
  end
  resources :licenses, only: %i[index show], param: :slug do
    resources :albums,   only: %i[index]
    resources :articles, only: %i[index]
    resources :notes,    only: %i[index]
    resources :projects, only: %i[index]
    resources :uploads,  only: %i[index]
  end
  resources :tags, only: %i[index show], param: :slug do
    resources :albums,   only: %i[index]
    resources :articles, only: %i[index]
    resources :links,    only: %i[index]
    resources :notes,    only: %i[index]
    resources :projects, only: %i[index]
    resources :uploads,  only: %i[index]
  end
  resources :users, only: %i[index show], param: :username do
    resources :albums,   only: %i[index]
    resources :links,    only: %i[index]
    resources :articles, only: %i[index]
    resources :notes,    only: %i[index]
    resources :projects, only: %i[index]
    resources :uploads,  only: %i[index]
  end

  # Reports
  post '/reports', to: 'reports#create', as: :create_report

  # Activity
  get '/activity', to: 'acts#index', as: :activities

  # Member
  namespace :member do
    resources :albums, except: %i[show], param: :slug do
      put 'toggle_publication', to: 'albums#toggle_publication', as: :toggle_publication
    end
    resources :articles, except: %i[show], param: :slug do
      put 'toggle_publication', to: 'articles#toggle_publication', as: :toggle_publication
    end
    resources :links, except: %i[show] do
      put 'toggle_publication', to: 'links#toggle_publication', as: :toggle_publication
    end
    resources :notes, except: %i[show], param: :slug do
      put 'toggle_publication', to: 'notes#toggle_publication', as: :toggle_publication
    end
    resources :projects, except: %i[show], param: :slug do
      put 'toggle_publication', to: 'projects#toggle_publication', as: :toggle_publication
    end
    resources :uploads, except: %i[show], param: :slug do
      put 'toggle_publication', to: 'uploads#toggle_publication', as: :toggle_publication
    end

    resources :notifications, only: %i[index destroy]

    resources :comments, only: %i[destroy] do
      put '/archive', to: 'comments#archive', as: :archive
    end

    get '/preferences', to: 'preferences#edit', as: :edit_preferences
    put '/preferences', to: 'preferences#update', as: :update_preferences

    get '/infos', to: 'users#edit', as: :edit_infos
    put '/infos', to: 'users#update', as: :update_infos

    post '/markdown_previewer', to: 'markdown_previewer#preview', as: :markdown_preview
  end

  # Admin
  namespace :admin do
    # Content resources
    resources :albums, only: %i[index destroy], param: :slug do
      put 'toggle_lock', to: 'albums#toggle_lock', as: :toggle_lock
    end
    resources :articles, only: %i[index destroy], param: :slug do
      put 'toggle_lock', to: 'articles#toggle_lock', as: :toggle_lock
    end
    resources :links, only: %i[index destroy] do
      put 'toggle_lock', to: 'links#toggle_lock', as: :toggle_lock
    end
    resources :notes, only: %i[index destroy], param: :slug do
      put 'toggle_lock', to: 'notes#toggle_lock', as: :toggle_lock
    end
    resources :projects, only: %i[index destroy], param: :slug do
      put 'toggle_lock', to: 'projects#toggle_lock', as: :toggle_lock
    end
    resources :uploads, only: %i[index destroy], param: :slug do
      put 'toggle_lock', to: 'uploads#toggle_lock', as: :toggle_lock
    end

    # Basic resources
    resources :announcements, except: %i[show]
    resources :languages,     except: %i[show], param: :iso639_1
    resources :licenses,      except: %i[show], param: :slug
    resources :reports,       only:   %i[index destroy]
    resources :tags,          except: %i[create show], param: :slug
    resources :users,         only:   %i[index show destroy], param: :username
  end
end
