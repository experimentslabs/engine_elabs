json.extract! act, :id,
              :content_id,
              :content_type,
              :event,
              :created_at,
              :content
