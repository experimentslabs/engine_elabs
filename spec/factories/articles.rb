FactoryBot.define do
  factory :article, class: Elabs::Article do
    title { Faker::Lorem.sentence }
    excerpt { Faker::Lorem.paragraph }
    content { Faker::Lorem.paragraphs number: 6 }
    user
    license
    language

    factory :article_published do
      published { true }
      published_at { Time.zone.now }

      factory :article_published_and_safe do
        sfw { true }
      end
    end
  end
end
