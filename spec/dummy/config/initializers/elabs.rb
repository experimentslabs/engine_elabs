Elabs.setup do |config|
  config.use_avatars = true
  config.exportable_markdown = true
end
