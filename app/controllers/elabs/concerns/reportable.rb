module Elabs
  module Concerns
    module Reportable
      extend ActiveSupport::Concern

      included do
        before_action :prepare_empty_report
      end

      private

      def prepare_empty_report
        @new_report = Elabs::Report.new
      end
    end
  end
end
