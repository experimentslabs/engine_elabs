# Tips and notes on customizing Elabs

## Site translations menu
If you want to dynamically display the links to change the
site language in the menu and you have the "Languages" of the
translations, you can override `elabs/layouts/_language_menu.html.haml`
with this content:

```haml
- Elabs::Language.available_site_translations.each do |l|
  = link_to l.name, { locale: l.iso639_1 }, lang: l.iso639_1
```

