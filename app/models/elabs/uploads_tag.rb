module Elabs
  class UploadsTag < ApplicationRecord
    self.table_name = 'uploads_tags'

    include Elabs::Concerns::CountablePivot

    COUNTABLE_DEFINITION = %i[uploads tag].freeze

    belongs_to :upload
    belongs_to :tag
  end
end
