require 'rails_helper'
require 'rspec_rails_api'

RSpec.configure do |config|
  config.include RSpec::Rails::Api::DSL::Example
end

renderer = RSpec::Rails::Api::OpenApiRenderer.new
renderer.api_servers = [{ url: 'https://example.com' }]
renderer.api_title = 'API to access ExperimentsLabs content'
renderer.api_version = '1'
renderer.api_description = 'Access and update data on this website'

# # Save the Swagger documentation
# RSpec.configuration.after(:context, type: :acceptance) do |context|
#   renderer.merge_context context.class.metadata[:rrad].to_h
# end
#
# RSpec.configuration.after(:suite) do
#   renderer.write_files Rails.root.join('public', 'swagger_doc'), only: [:yaml]
# end
