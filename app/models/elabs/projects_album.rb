module Elabs
  class ProjectsAlbum < ApplicationRecord
    self.table_name = 'projects_albums'

    include Elabs::Concerns::CountablePivot

    COUNTABLE_DEFINITION = %i[albums project].freeze

    belongs_to :project
    belongs_to :album
  end
end
