Feature: Links management
  As a website maintainer,
  In order to have a safe content
  I want to lock or delete user submitted links

  Background:
    Given I am an administrator
    And a website with a "fr" language for content in "Français"
    And a website with one sfw, published link titled "My first link"

  Scenario: Admin lock link
    Given I lock the "My first link" link
    When I visit the public links page
    Then I should not see the "My first link" in the public links list
