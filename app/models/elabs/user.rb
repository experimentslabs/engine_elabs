module Elabs
  class User < ApplicationRecord
    self.table_name = 'users'
    include Elabs::Concerns::Sluggable

    SLUGGABLE_FIELD = nil
    SLUG_FIELD      = :username

    validates :username, presence: true, uniqueness: true
    validates :role, presence: false
    validate :avatar_format if Elabs.use_avatars

    # Username should:
    # - not start with '-', '_' or '.'
    # - contain only a to z characters (upper/lower cased), numbers, '.', '-' or '_'
    # - not contain successive '.', '-' or '_'
    validates :username, format: { with: /\A[a-zA-Z0-9]+([_-]?[a-zA-Z0-9]+)?\z/ }
    validates :username, length: { minimum: 4 }

    has_many :albums,         dependent: :destroy
    has_many :articles,       dependent: :destroy
    has_many :links,          dependent: :destroy
    has_many :notes,          dependent: :destroy
    has_many :projects,       dependent: :destroy
    has_many :uploads,        dependent: :destroy
    has_many :reports,        dependent: :destroy
    has_many :notifications,  dependent: :destroy
    has_one  :preference, dependent: :delete
    # Public filters
    has_many :public_albums,   -> { publicly_visible }, class_name: 'Elabs::Album',   inverse_of: :user
    has_many :public_articles, -> { publicly_visible }, class_name: 'Elabs::Article', inverse_of: :user
    has_many :public_links,    -> { publicly_visible }, class_name: 'Elabs::Link',    inverse_of: :user
    has_many :public_notes,    -> { publicly_visible }, class_name: 'Elabs::Note',    inverse_of: :user
    has_many :public_projects, -> { publicly_visible }, class_name: 'Elabs::Project', inverse_of: :user
    has_many :public_uploads,  -> { publicly_visible }, class_name: 'Elabs::Upload',  inverse_of: :user

    has_one_attached :avatar if Elabs.use_avatars

    scope :admins, -> { where(role: 'admin') }

    after_create do
      Preference.create! user: self, show_nsfw: false
    end

    def admin?
      role == 'admin'
    end

    def display_name
      real_name.presence || "@#{username}"
    end

    def to_param
      username
    end

    private

    def avatar_format
      return unless avatar.attached? # && file.blob
      return if %w[image/png image/jpeg].include? avatar.blob.content_type

      avatar.purge_later
      errors.add(:avatar, I18n.t('elabs.model.user.needs_to_be_a_png_or_jpg_image'))
    end
  end
end
