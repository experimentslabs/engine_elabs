json.content do
  json.array! content, partial: partial, as: as
end
json.per_page content.current_per_page
json.pages content.total_pages
json.total content.total_count
json.page content.current_page
