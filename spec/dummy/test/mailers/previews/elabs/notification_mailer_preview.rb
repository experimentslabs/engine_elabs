module Elabs
  # Preview all emails at http://localhost:3000/rails/mailers/elabs/notification_mailer
  class NotificationMailerPreview < ActionMailer::Preview
    # Preview at http://localhost:3000/rails/mailers/elabs/notification_mailer/comment_notification_email
    def comment_notification_email
      notification = Elabs::Notification.where(content_type: 'Elabs::Comment').first
      NotificationMailer.with(notification: notification).comment_notification_email
    end

    # Preview at http://localhost:3000/rails/mailers/elabs/notification_mailer/lock_notification_email
    def lock_notification_email
      content = Elabs::Album.first
      notification = Elabs::Notification.new(
        content:     content,
        user:        content.user,
        source_user: User.where(role: 'admin').first,
        event:       'lock',
        message:     'Your content has been locked because it seems unfinished.'
      )
      NotificationMailer.with(notification: notification).lock_notification_email
    end

    # Preview at http://localhost:3000/rails/mailers/elabs/notification_mailer/report_notification_email
    def report_notification_email
      notification = Elabs::Notification.where(content_type: 'Elabs::Report').first
      NotificationMailer.with(notification: notification).report_notification_email
    end

    private

    def subject_element
      user = Elabs::User.where(role: 'user').first
      FactoryBot.create :album_published,
                        user:     user,
                        language: Elabs::Language.first,
                        license:  Elabs::License.first
    end
  end
end
