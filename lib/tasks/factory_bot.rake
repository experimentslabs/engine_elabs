namespace :factory_bot do
  desc 'Verify that all FactoryBot factories are valid'
  task lint: :environment do
    if Rails.env.test?
      require 'database_cleaner'
      DatabaseCleaner.cleaning do
        FactoryBot.lint
      end
    else
      system("RAILS_ENV='test' bundle exec rake app:factory_bot:lint")
      raise if $CHILD_STATUS.exitstatus.nonzero?
    end
  end
end
