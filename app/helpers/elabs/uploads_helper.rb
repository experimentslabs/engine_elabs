module Elabs
  module UploadsHelper
    def playable?(uploaded_file)
      video?(uploaded_file) || audio?(uploaded_file)
    end

    def audio?(uploaded_file)
      Elabs.av_formats_audio.include? uploaded_file.content_type
    end

    def video?(uploaded_file)
      Elabs.av_formats_video.include? uploaded_file.content_type
    end

    def available_formats
      Elabs.av_formats_audio + Elabs.av_formats_video
    end
  end
end
