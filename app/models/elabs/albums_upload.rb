module Elabs
  class AlbumsUpload < ApplicationRecord
    self.table_name = 'albums_uploads'

    include Elabs::Concerns::CountablePivot

    COUNTABLE_DEFINITION = %i[uploads album].freeze

    belongs_to :album
    belongs_to :upload
  end
end
