require 'simplecov'

require 'capybara-screenshot/cucumber'
require 'selenium-webdriver'

Capybara.register_driver(:selenium) do |app|
  browser_options = {
    'chrome'          => { browser: :chrome },
    'chrome-headless' => { browser: :chrome, options: Selenium::WebDriver::Chrome::Options.new(args: %w[headless no-sandbox]) },
    'firefox'         => { browser: :firefox }
  }

  browser = ENV['CAPYBARA_DRIVER'] || 'firefox'

  Capybara::Selenium::Driver.new(app, browser_options[browser])
end

Capybara.javascript_driver = :selenium
Capybara.save_path         = File.join('tmp', 'capybara-screenshots')
