# Change Log
All notable changes to this project will be documented in this file.

## [5.0.0] - 2018-12-31 - Announcements, redesign

Updating to this version **breaks** the original style and layout.

### Added

  - New "license-icons" webfont to provide licenses icons.
  - `icon` helper now have a new `pack` parameter, which allows to specify
    a custom icon pack.
  - A `elabs:images` generator has been added, to copy images for override
  - Admins can make announces for specific targets (global, members or admins)
    that are displayed in public and member layouts. Announcements are
    dismissible, this information being stored in localStorage.
  - A _really_ simple system to avoid dumb bots on comments and reports:
    an additional field is added to the form, the user is asked to leave
    it blank. The field is hidden by JS on page load, for people who use
    JS everywhere. If the field is filled, a success message is displayed
    but nothing is actually saved. This can be disabled with the
    `Elabs::trap_dumb_bots` option in the initializer.
  - JS helpers: new `hasClass(element, className)` and
    `toggleClass(element, className)` methods
  - SCSS elements: ability to create dropdown menus

### Changed

  - DeviseViews generator is now DeviseMailerViews, as it's what it does.
  - Helpers are split in more files for more sense.
  - The way comment notifications are managed is changed: notification now
    points to the comment. All existing comment notification are deleted.
  - Shortcodes for content types and users now uses slugs

### Improved

  - Fonts an images are no longer in "public" folder. They have been moved
    in `app/assets` and are now processed by the assets pipeline
  - Shortcode helpers were improved, and tests are now more precise on the
    shortcodes behaviors
  - Cards for content types are now a generic partial. Edit
    `layout/_content_card` to override it
  - "Related content" areas with tabs are now a generic partial. Edit
    `layout/_content_associations` to override them.
  - Forms for content now have a `_form_commons` partial for generic inputs
  - Comments:
    - Comments can be archived and deleted
    - Associated notification are removed on delete/archive
  - Notifications links displays content title for comments notifications.
  - Notifications are deleted when reports are destroyed
  - All SCSS files are now sorted in better sub-folders:
    - "app/admin-members" stays the same for now
    - "app/components" for styles applied to whole _widgets_
    - "app/elements" for standalone elements (i.e: button, form, badge,
      ...)
    - "app/helpers" for elements to be extended (i.e: `%clickable`
    - "app/layout" for layout elements (i.e: pages, typo, ...)
    - "app/mixins" stays the same.
  - Overall style was enhanced:
    - Responsive design
    - More space around elements
    - Dropdowns menu
    - Popping panels
    - ...
  - Variables were extracted from SCSS components and put in a big
    `_variables.scs` file
  - Language page now only shows languages with content
  - In "Related content" areas:
    - the default tab is now the first with content
    - the "Show all" link is disabled when there is no related content
  - All Ruby and Node dependencies were updated

### Removed

  - Yarn command `sass:lint-fix` is no longer available: stylelint ignores
    the inline "disable" rules when autofixing; resulting in a total mess
    when executed.

### Fixed

  - Licenses icons are removed from `/licenses/xxx` page titles, so no
    html is inserted in the layout title

## [4.0.0] - 2018-10-11 - RSS, slugs ans shortcodes
### Added

  - RSS feeds for content and activity
  - Added `sources_url` and `docs_url` fields in projects
  - Support for shortcodes:
    - In Elabs:
        - `[<content>:<id>]` displays a link with an icon to the specified
          content,
        - `[<content>-card:<id>]` displays a card of the specified content
        - `[user:<id>]`, `[user-card:<id>]` displays link with icon or a card
          for the specified user
    - Gitlab instances (_Only https, public data are handled for now.)_:
        - `[gitlab-repo:<url>]`, will display a link with an icon to the
          repository
        - `[gitlab-repo-card:<url>]` displays a card for a repository
        - `[gitlab-user-card:<url>]` displays a card for a gitlab user
        - `[gitlab-group-card:<url>]` displays a card for a gitlab user
    - Github:
      - `[github-repo:<username/repo>]`, displays a link with an icon
        to the repository
      - `[github-repo-card:<username/repo>]` displays a card for a
        repository
      - `[github-user-card:<username>]` displays a card for a github user
  - Added a `with_format` helper method to render a partial of a different
    format in a view (i.e.: `with_format(:html){ markdown 'content' }` in
    a json view.
  - Added a `colored_good_or_bad_icon_tag` that colors the icon depending on
    its value.
  - Added classes for text based on the list of color variant.
    `is-<variant>` colors the text with the background-color defined in
    `$variants-list`
  - Added ability to save content specifying a reason for the edition.
    This reason will be saved in the corresponding act.
  - Added ability to save content specifying the modification as "minor".
    No act will be created in this case.
  - Slug support added. This **is a breaking change** as it will change all
    the links in the site. The following entities are now slugged:
    - languages/licenses/tags/users classifiers and their nested
      resources (i.e: `/languages/en/projects`)
    - albums/articles/notes/projects/uploads and projects nested resources
  - Customized datetime formats were added. Sadly, it's in yml files
    located in `config/locales/<lng>.yml` until we can export this to
    gettext.

### Changed

  - Markdown is now rendered with gem `commonmarker`. Markdown-it has
    been removed, so content previews are done with an ajax request
  - `boolean_icon_tag()` now takes two additional parameters:
    - `false_class`: additional CSS class when value is `false`. Default is `nil`
    - `true_class`: additional CSS class when value is `true`. Default is `nil`

### Improved

  - `Elabs::ActsHelper.act_notice_string` now have a second parameter,
    `include_link`. Default is `true`; set it to false to generate
    activity notice strings without link in it.
  - Admin/member indexes: disabled "Show online" links when content is
    not publicly visible.
  - Improved thumbnails generation methods with a new
    `thumbnail_center_crop(document, size)` which will return
    the URL for processed document.
  - Avatar and thumbnails possible dimensions are now
    `THUMBNAILS_DIMENSIONS` and `AVATAR_DIMENSIONS` module constants.
  - User cards are now larger and content is condensed on one line.
  - Member and admin indexes now have colored status icons
  - Visual enhancements:
    - rendered content have more space between sections; titles are more
      contrasted
    - cards are more contrasted
    - actions zones in forms are now standing out
    - tabs for related content now have the count of related items in them
    - `.field` classes now have a top margin, resulting in more airy forms
  - `short_date` helper method now displays a ... date.
  - `medium_datetime` helper method has been created to display a short
    datetime with the year in it.

### Removed

### Fixed

  - The permalink icon was missing on notes cards

## [3.0.0] - 2018-09-17 - Notifications and avatars
### Added

  - User model:
    - new `admins` scope to find all admins
    - new `display_name` method to return the real name or username as
      fallback
    - Experimental avatar support (ActiveStorage attachment `avatar`).
      **NOTE** that there are issues with uploads validation
      ([ActiveStorage validations are not supported for now](https://stackoverflow.com/questions/51872521/how-to-prevent-active-storage-updating-model-when-validation-fails)),
      leading to the uploaded file being saved even if validation fails.
      To prevent this, the attached file is deleted on failure,
      resulting on avatar suppression.To disable avatar support, set
      `Elabs::use_avatars` to false in the initializer.

  - Notifications:
    - MVC for simple notification system
    - Updated reports/comments to use notifications on save. Updated
      admin actions as "lock", "unlock", "delete" to notify the authors
    - New `NotifiableEntity` concern for content models

  - Misc:
    - Date helper: new `short_date(date)` method
    - Registrations can be closed by changing the `Elabs::users_can_register`
      variable. All it does is to basically prevent `:registerable` in the
      user model.

### Changed

  - Views:
    - `members/layout/_empty`: button to create content is now optional
    - Updated views to use `user.display_name` when needed
    - License icon is now a balance-scale icon
    - Short dates
    - Separators between information types on "show" views
    - Reordered menus
  - `ActableEntity` concern: moved `current_publish_action`,
    `current_lock_action` and `current_update_action` to the
    `ContentEntity` concern, as they are now shared between "actable"
    and "notifiable" concerns
  - `rails g elabs:install` now creates an user model template with devise
    configured with conditional behavior.

### Improved

  - Factorized some helper methods from "admin_content_helper" and
    "member_content_helper" in "content_helper"
  - Methods for public controllers are now in a `ElabsPublicController`.
    All custom public controller should extend this new controller instead
    of `ElabsApplicationController`
  - Improved `xxx_selector` methods to be able to specify the field name
    and the `multiple` select variant.

### Removed

### Fixed

  - Fixed `current_publish_action` and `current_lock_action`: they now
    return `:nothing` if content is already unpublished or locked.
  - Sort order in public views for users, tags, languages and licenses

## [2.0.1] - 2018-09-10 - JSON views
### Added

  - Added "Active" and "Password reset" columns in admin users list to
    have a better understanding of the users statuses

### Improved

  - Reviewed and updated all JSON views for future use in ajax requests

### Removed

  - Removed the timestamps from the licenses table. You'll need to
    create a migration that drops `created_at` and `updated_at` on the
    `licenses` table

## [2.0.0] - 2018-09-08 - Rails engine
Everything seems to run fine, it's enough for a major version.

### Added

  - Added missing ability to set content as "hidden in history":
    following updates and activity on an "hidden" element will not be
    logged in activity table.

## [2.0.0.pre] - 2018-09-07 - Rails engine
### Added

  - Generators to make views/assets override easy
  - Generator to copy needed files and initializer
  - Two controller concerns to easily extend your custom controllers:
    - "ElabsController", used to load Elabs layout and some helpers
    - "Reportable", to prepare an empty report when using elabs layout

### Changed

  - The app is now an engine.

### Improved

  - New documentation
  - Various improvements:
    - Acts strings for notices are now in the "acts" helper

### Fixed
  - Translations of acts notices are now fixed

## [1.0.0] - 2018-08-09 - First v1 \o/
### Added

  - Updated translations
  - Added methods to access the elabs configuration quickly

### Changed

  - A lot of methods were returning constant strings. They are now constants.

### Improved

  - Improved configuration
  - Improved setup
  - Improved README

## [0.15.0] - 2018-08-05 - "Tests improvements"
### Added

  - Shared examples for RSpec controllers tests
  - Completed the maximum of cucumber steps tagged with `@wip`
  - Option to enable sendmail email transport

### Changed

  - Capybara screenshots are now in `tmp/`

### Improved

  - Renamed and moved Cucumber's features and steps
  - Refactored some of the existing Cucumber steps
  - Updated all controller tests to use shared examples

### Removed

  - Removed unused method "validate_related_data" in `content_entity.rb`

### Fixed

  - Fixed the relation validator which was not working and badly tested

## [0.14.0] - 2018-08-04 - "Markdown preview"
### Added

  - Content textareas are now previewable

### Changed

  - Replaced RedCarpet by Markdown-it, a JS markdown parser. The choice
    has been made to render content on the client side to have the same
    renderer as in previews. Additionally, Highlight.js was added to
    render code blocks.

## [0.13.0] - 2018-08-03 - "Improvements"
### Added

  - Added Brakeman to CI to check for known vulnerabilities
  - Added HTML5 audio/video previews
  - Created a configuration file for the app's config, `config/elabs.yml`
  - Placeholders when there is no content to display

### Changed

### Improved

  - Added thumbnails helper with methods returning properties for thumbnail
    generation.
  - Made the generated files variant saved
  - Made counter caches take care of the state of the entity (draft/locked)
  - Refactored 'application_content_record' model in multiple concerns

### Removed

  - "Pages" controller, as it was not used anymore.

### Fixed

  - Updated failing steps by specifying username and email: creating an
    user from email only don't allow to target it simply, randomly
    leading to wrong user being clicked on during tests
  - Fixed public content that can be accessed even when it's a draft.
    (locked content are still found, control is made in the view)

## [0.12.0] - 2018-07-30 - "Design"
### Added

### Changed

  - `rake wipe:db` is now `rake wipe:db_seed`. `rake wipe:db` will only drop/create/migrate
  - Created general layout style
  - Created style for public/member/admin views:
    - albums
    - articles
    - notes
    - projects
    - uploads
    - languages/licenses/tags
    - users
  - Added CodeMirror for editable textareas
  - Added helpers and links to filter/order content on indexes

### Improved

### Removed

### Fixed

  - Fixed scopes "for_relation" which was badly constructed, leading to
    incorrect content being fetched

## [0.11.0] - 2018-07-15 - "Acts": content actions history
### Added

  - Actions on generic content are now logged in the `acts table`

### Changed

  - "root" route is now `acts#index`

## [0.10.0] - 2018-07-14 - Comment system
### Added

  - Simple comment system

### Improved

  - Public controllers related to content now extends a
    `ContentApplicationController` instead of the `ApplicationController`

## [0.9.0] - 2018-07-14 - Preferences
### Added

  - Members now have preferences for the default locale and to display
    or not the NSFW content

## [0.8.0] - 2018-07-14 - Toggle SFW/NSFW
### Added

  - NSFW content is hidden
  - Users can now toggle the NSFW content visibility

### Fixed

  - Updated notes table, setting default values for `sfw` and
    `hidden_in_history` fields

## [0.7.0] - 2018-07-11 - Lock/unlock content
### Added

  - Admin:
    - actions to lock/unlock content
  - Members:
    - actions to publish/unpublish content

### Improved

  - Admin:
    - all content controllers now extends an unique controller which contains all the shared methods
  - Member
    - all content controllers now extends an unique controller which contains all the shared methods

### Fixed
  - Issues with FactoryBot seems fixed (waiting for the Faker PRs to be merged)
    - Faker PR 1307: [Add a "add_to_previous" method for `UniqueGenerator`](https://github.com/stympy/faker/pull/1307)
    - Faker PR 1302: [Add `Faker::Alphanum` generator](https://github.com/stympy/faker/pull/1302)

## [0.6.0] - 2018-07-09 - Nested content and pagination
### Added

  - List of the 10 last elements related to a given content
  - Pagination for all indexes (except tags and licenses)
  - Filters for nested resources
  - PostgreSQL support (gem and sample config file)

### Changed

  - Database used on CI is now PostgreSQL

### Improved

  - Content in development seeds for the administrator
  - Faker's "unique" status is reset before every Cucumber scenario and RSpec examples
  - Installer now copies database configuration files.

### Fixed

  - Tests failing due to locked database

## [0.5.0] - 2018-06-29 - Relations and counters
### Added

  - Tags support for albums/articles/notes/projects/uploads
  - Relations counts on languages/tags/licenses/albums/projects/users
  - Update tags show view to display the last related content
  - Update languages show view to display the last related content
  - Update licenses show view to display the last related content

### Changed

  - Changed content's habtm relations to `has_many through`

### Improved

  - Improved some cucumber steps

## [0.4.0] - 2018-06-29 - CRUD MVC for content
### Added

  - Articles basic MVC for public/member/admin zones
  - Notes basic MVC for public/member/admin zones
  - Uploads basic MVC for public/member/admin zones
  - Albums basic MVC for public/member/admin zones
  - Projects basic MVC for public/member/admin zones
  - Member layout

### Improved

  - Improved factories to only use Faker, specialization comes after
  - Added `factory_bot:lint` rake task to test factories reusability

## [0.3.1] - 2018-06-17 - Fixes, licenses
### Added

  - Licenses support: Public/admin MVC and cucumber tests

### Improved

  - Updated controller with missing translation calls
  - Changed Rubocop Metrics/ABCSize to 16 to allow translations in basic actions

### Fixed

  - Reports required fields

## [0.3.0] - 2018-06-17 - Languages, tags and reports
### Added

  - Language support: Public/admin MVC and cucumber tests
  - Tags support: Public/admin MVC and cucumber tests
  - Admin layout

### Fixed

  - Simplecov is now configured

## [0.2.1] - 2018-06-12 - Tests
### Added

  - Added Cucumber scenarios and steps to test language switching

### Improved

  - Removed "fuzzy" comments in french translations

## [0.2.0] - 2018-06-12 - Site translations
### Added

  - Added mechanism to change the website's language using Gettext
  - Created the configuration files
  - Created french translations

## [0.1.0] - 2018-06-11 - Base setup
### Added

  - Readme
  - Changelog
  - Code of conduct
  - Basic Cucumber features, scenarios and tests
  - Devise setups
  - Basic users listing
