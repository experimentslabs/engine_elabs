# Additional license information:

- `Woody.jpg`, located in `spec/fixtures/files` is under
  [CreativeCommons by-nc-sa 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/),
  and was created by Manuel Tancoigne

