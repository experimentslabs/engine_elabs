# frozen_string_literal: true

require 'rails/generators/base'

module Elabs
  module Generators
    class InstallGenerator < Rails::Generators::Base
      source_root File.expand_path('../../..', __dir__)

      def set_source_paths
        @source_paths = [
          File.expand_path('../templates', __dir__),
          File.expand_path('../../..', __dir__)
        ]
      end

      desc 'Creates the "elabs" initializer and copies needed files'

      def copy_files
        template 'config/initializers/elabs.rb'
        template 'app/models/user.rb'
        template 'config/initializers/locale.rb'
        template 'config/locales/en.yml', 'config/locales/elabs.en.yml'
        template 'config/locales/fr.yml', 'config/locales/elabs.fr.yml'
      end
    end
  end
end
