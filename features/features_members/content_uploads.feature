Feature: upload management
  As a member
  In order to manage the content I publish
  I want to be able to create or update the upload's content

  Background:
    Given I am a member with email "bob@example.com"
    And a website with a "Creative Commons by" license
    And a website with a "fr" language for content in "Français"
    And a website with one sfw, published upload, "My first upload" which I created

  Scenario: Create upload
    Given I create an upload with title "This should be on Instagram"
    Then I should see the upload containing "This should be on Instagram" in the member zone
    And I should see a list of 2 uploads

  Scenario: Update upload
    Given I change upload "My first upload" to "Space rocket: schemas"
    Then I should see the upload containing "Space rocket: schemas" in the member zone
    And I should see a list of 1 upload
