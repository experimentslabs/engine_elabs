module Elabs
  class AlbumsTag < ApplicationRecord
    self.table_name = 'albums_tags'

    include Elabs::Concerns::CountablePivot
    COUNTABLE_DEFINITION = %i[albums tag].freeze

    belongs_to :album
    belongs_to :tag
  end
end
