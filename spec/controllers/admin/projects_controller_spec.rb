require 'rails_helper'

RSpec.describe Elabs::Admin::ProjectsController, type: :controller do
  model            = Elabs::Project
  valid_session    = {}

  it_behaves_like 'admin index entity', model, valid_session
  it_behaves_like 'admin destroy entity', model, valid_session
  it_behaves_like 'admin lock content', model, valid_session
  it_behaves_like 'admin destroy content', model, valid_session
end
