# Installation

## Pre-requisites

- ruby 2.5
- PostgreSQL (not tested with Mysql, not usable with sqlite in the
  current state)
- NodeJS 8 and Yarn **if** you want to use the default views
- ImageMagick and ffmpeg **if** you want to use the default views

Additionally, to compile some native ruby extensions, you will need some
development libraries, as, for an ubuntu 18.04: `build-essential`,
`libpq-dev`, `libxml2-dev`

## On a new Rails app
Create a new Rails application using the Rails generator:

```shell
rail new myapp -d postgresql
cd myapp
```

### Gems

Add Elabs to the `Gemfile`:

```rb
gem 'elabs'
```

Execute `bundle install` to install it.

### Setup Devise

Devise is the only authentication solution supported by Elabs for now.
The model used for users **have to be `User`**.

_Following the [Devise documentation](https://github.com/plataformatec/devise#getting-started):_

- You need to run the generator: `rails generate devise:install`.
- At this point, a number of instructions will appear in the console.
  Among these instructions, you'll need to set up the default URL options
  for the Devise mailer in each environment. Here is a possible
  configuration for `config/environments/development.rb`:
  `config.action_mailer.default_url_options = { host: 'localhost', port: 3000 }`.
  The generator will install an initializer which describes ALL of
  Devise's configuration options. It is *imperative* that you take a
  look at it. When you are done, you are ready to add Devise to any of
  your models using the generator.
- Generate the User model: `rails generate devise User`
  The generator also configures your `config/routes.rb` file to point
  to the Devise controller, but we will overwrite it later.
- Next, check the User model for any additional configuration options
  you might want to add, such as confirmable or lockable. If you add an
  option, be sure to inspect the migration file (created by the
  generator if your ORM supports them) and uncomment the appropriate
  section. For example, if you add the confirmable option in the model,
  you'll need to uncomment the Confirmable section in the migration.
- Edit these two values in `config/initializers/devise.rb`:
    - `config.mailer = 'Elabs::DeviseMailer'`
    - `config.parent_mailer = 'Elabs::ApplicationMailer'`

### Setup Elabs
Once Devise is set up, change the `User` model to extend `Elabs::User` instead
of `ApplicationRecord`.

Elabs comes with rake tasks to quickly get ready:

- Copy migrations: `rails elabs:install:migrations`, will copy all the
  migrations in `db/migrate`
- Copy initializer and other required files: `rails generate elabs:install`.
  Accept to overwrite the `package.json` file **if** you want to use the
  default views.
- Edit `config/database.yml` to suit your needs.
- Run `rails db:create` and `rails db:migrate`

If you want to use the default views, you have to copy some assets from
elabs:

- Copy required assets: `rails g elabs:required_assets`

### Routes

Update `config/routes.rb` to look like this:

```rb
# config/routes.rb
Rails.application.routes.draw do
  # Elabs routes. Replace '/' by any prefix, depending on what you want.
  mount Elabs::Engine, at: '/'

  # Home page (or whatever suits your needs. I.e.: 'elabs/projects#index', ...)
  root 'elabs/acts#index'
end
```

### Default views and assets
If you want to use the default views, you'll need to install the node
dependencies with yarn: `yarn install`

Update application manifests to use Elabs assets too:

```rb
# app/assets/javascripts/application.js
// [...]
//= require elabs/application
```

```rb
# app/assets/stylesheets/application.css
 * [...]
 *= require elabs/application
```

### Create an administrator
Admins are people with `admin` role. Open a Rails console and create your
first user.
```rb
User.create username: 'admin', role: 'admin', email: 'admin@yoursite.com', password: 'SomePassword'
```

An other way is to create an account from the web interface and edit it
in the rails console:

```rb
User.last.update(role: 'admin')
```

### Done !

Start to develop your site !

```rb
rails s
```

Now, login with the admin account and create at least a language and a license.

## On an existing Rails app
**This section needs feedback :)**
