Feature: Content language
  As a maintainer
  In order to allow creators to write in their native language
  I want to be able to manage languages attachable to contents

  Background:
    Given I am an administrator
    And a website with a "fr" language for content in "Français"

  Scenario: Admin display languages
    Given I visit the admin languages page
    Then I should see a list of languages

  Scenario: Admin update languages
    Given I change a language to "de", "Deutsch"
    Then The language should now have the "de", "Deutsch" values

  Scenario: Admin create languages
    When I create the "Русский" language with "ru" code
    Then I should see the "ru", "Русский" language in the list

  Scenario: Admin delete languages
    When I delete the "Français" language
    Then I should see that the "Français" language is not present in the list
