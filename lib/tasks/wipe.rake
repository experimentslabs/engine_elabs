# rubocop:disable Metrics/BlockLength
namespace :wipe do
  desc 'Deletes development data'
  task all: :environment do
    abort "You're not in development environment. Aborting." unless Rails.env.development?
    system('bundle exec rake wipe:db')
    system('bundle exec rake wipe:logs')
    system('bundle exec rake wipe:files')
  end
  task db: :environment do
    abort "You're not in development environment. Aborting." unless Rails.env.development?
    system('bundle exec rake db:drop')
    system('bundle exec rake db:create')
    system('bundle exec rake db:migrate')
  end
  task db_seed: :environment do
    abort "You're not in development environment. Aborting." unless Rails.env.development?
    system('bundle exec rake wipe:db')
    system('bundle exec rake db:seed')
  end
  task logs: :environment do
    abort "You're not in development environment. Aborting." unless Rails.env.development?
    system("rm -f #{Rails.root.join('log', '*.log')}")
  end
  task files: :environment do
    abort "You're not in development environment. Aborting." unless Rails.env.development?

    system("rm -rf #{Rails.root.join('storage')}")
    system("rm -rf #{Rails.root.join('tmp', 'storage')}")
  end
end
# rubocop:enable Metrics/BlockLength
