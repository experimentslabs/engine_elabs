When(/^I create an? (global|member|admin) announcement$/) do |target|
  visit '/admin/announcements/new'
  target[0] = target[0].capitalize
  select target, from: 'Target'
  fill_in 'Content', with: 'Some fine piece of information'

  click_on 'Save'
end

When(/^I create a announcement for (last|next) month$/) do |date|
  now = Date.new
  start_at = date == 'last' ? now - 30 : now + 30
  end_at   = date == 'last' ? now - 3 : now + 33
  FactoryBot.create :announcement, start_at: start_at, end_at: end_at
end

Then(/^I should (see|not see) the announcement on the homepage$/) do |visibility|
  count = visibility == 'see' ? 1 : 0
  visit '/'

  expect(all('.announcement').count).to eq(count)
end

Then(/^I should (see|not see) the announcement on the members section$/) do |visibility|
  count = visibility == 'see' ? 1 : 0
  visit '/member/notifications'

  expect(all('.announcement').count).to eq(count)
end

Then(/^I should (see|not see) the announcement on the admin section$/) do |visibility|
  count = visibility == 'see' ? 1 : 0
  visit '/admin/languages'

  expect(all('.announcement').count).to eq(count)
end
