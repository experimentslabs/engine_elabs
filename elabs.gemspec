$LOAD_PATH.push File.expand_path('lib', __dir__)

# Maintain your gem's version:
require 'elabs/version'

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = 'elabs'
  s.version     = Elabs::VERSION
  s.authors     = ['Manuel Tancoigne']
  s.email       = ['m.tancoigne@gmail.com']
  s.homepage    = 'https://experimentslabs.com'
  s.summary     = 'ELabs is a project-centric Rails engine CMS'
  s.description = 'This Rails engine provides a CMS to manage content types as projects, articles, albums,...'
  s.license     = 'MIT'
  s.metadata    = {
    'source_code_uri' => 'https://gitlab.com/experimentslabs/engine_elabs',
    'bug_tracker_uri' => 'https://gitlab.com/experimentslabs/engine_elabs/issues',
    'changelog_uri'   => 'https://gitlab.com/experimentslabs/engine_elabs/blob/master/CHANGELOG.md'
  }

  s.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  s.require_paths = ['lib']

  s.add_dependency 'rails', '~> 5.2.1'
  # App gems
  s.add_dependency 'active_link_to'
  s.add_dependency 'commonmarker'
  s.add_dependency 'devise'
  s.add_dependency 'devise-i18n'
  s.add_dependency 'haml-rails'
  s.add_dependency 'kaminari'
  s.add_dependency 'mini_magick'
end
