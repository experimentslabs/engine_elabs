# Overriding Elabs engine

## Views
To override views, run `rails g elabs:views` and
`rails g elabs:devise_views`. This will copy all the views in
`app/views/elabs`. You will be able to edit them.

If you want to override the style but keep the views, you can run
`rails g elabs:assets`. This will copy CSS and JS files in
`app/assets/javascripts/elabs` and `app/assets/stylesheets/elabs`.
You will need the [Knacss](https://www.knacss.com) framework and other
things from node modules. Install them with yarn (`package.json` should
have been copied during the [installation](setup.md).

## Models, controllers
Sadly, we have not tested those parts for now. It would require some work
to complete the initializer to be able to use custom models; and a work
on the routes system, to only include the routes you need.

Contributions are welcome.
