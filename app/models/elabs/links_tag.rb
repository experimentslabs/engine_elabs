module Elabs
  class LinksTag < ApplicationRecord
    self.table_name = 'links_tags'

    include Elabs::Concerns::CountablePivot

    COUNTABLE_DEFINITION = %i[links tag].freeze

    belongs_to :link
    belongs_to :tag
  end
end
