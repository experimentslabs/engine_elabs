Feature: Album management
  As a member
  In order to manage the content I publish
  I want to be able to create or update the album's content

  Background:
    Given I am a member with email "bob@example.com"
    And a website with a "Creative Commons by" license
    And a website with a "fr" language for content in "Français"
    And a website with one sfw, published album, "My first album" which I created

  Scenario: Create album
    Given I create an album with title "Cats"
    Then I should see the album containing "Cats" in the member zone
    And I should see a list of 2 albums

  Scenario: Update album
    Given I change album "My first album" to "Space X - Blueprints"
    Then I should see the album containing "Space X - Blueprints" in the member zone
    And I should see a list of 1 album
