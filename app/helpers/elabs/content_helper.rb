module Elabs
  module ContentHelper
    def show_item?(entity)
      (entity.sfw? || show_nsfw?) && !entity.locked?
    end

    def user_is_author_of(entity)
      current_user&.id && entity.user_id == current_user.id
    end

    def first_type_with_content(entity)
      %i[albums articles notes projects uploads].each do |type|
        return type.to_s if entity.respond_to?(type) && entity["#{type}_count"].positive?
      end
      'albums'
    end
  end
end
