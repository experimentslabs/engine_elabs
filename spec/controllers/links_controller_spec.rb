require 'rails_helper'

RSpec.describe Elabs::LinksController, type: :controller do
  model               = Elabs::Link
  valid_nested_routes = %w[user tag language]
  valid_session       = {}

  it_behaves_like 'public index content', model, valid_session, valid_nested_routes
end
