require 'rails_helper'

RSpec.describe Elabs::Admin::ReportsController, type: :controller do
  model            = Elabs::Report
  valid_session    = {}

  it_behaves_like 'admin index entity', model, valid_session
  it_behaves_like 'admin destroy entity', model, valid_session
end
