FactoryBot.define do
  factory :upload, class: Elabs::Upload do
    title { Faker::Lorem.sentence }
    description { Faker::Lorem.paragraphs }
    language
    user
    license

    after(:build) do |upload|
      upload.file.attach(io:           File.open(File.join(File.expand_path('../fixtures/files', __dir__), 'Woody.jpg')),
                         filename:     'Woody.jpg',
                         content_type: 'image/jpeg')
    end

    factory :upload_published do
      published { true }
      published_at { Time.zone.now }

      factory :upload_published_and_safe do
        sfw { true }
      end
    end
  end
end
