module Elabs
  class ApplicationContentRecord < ApplicationRecord
    include Elabs::Concerns::ContentEntity
    include Elabs::Concerns::ActableEntity
    include Elabs::Concerns::CountableEntity
    include Elabs::Concerns::NotifiableEntity
    include Elabs::Concerns::Taggable
    include Elabs::Concerns::Sluggable

    SLUG_FIELD  = :slug
    ACT_UPDATES = true

    self.abstract_class = true
  end
end
