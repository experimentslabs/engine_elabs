require 'rails_helper'

RSpec.describe Elabs::LanguagesController, type: :controller do
  model               = Elabs::Language
  valid_nested_routes = []
  valid_session       = {}

  it_behaves_like 'public index entity', model, valid_session, valid_nested_routes
  it_behaves_like 'public show entity', model, valid_session
end
