module Elabs
  class ProjectsTag < ApplicationRecord
    self.table_name = 'projects_tags'

    include Elabs::Concerns::CountablePivot

    COUNTABLE_DEFINITION = %i[projects tag].freeze

    belongs_to :project
    belongs_to :tag
  end
end
