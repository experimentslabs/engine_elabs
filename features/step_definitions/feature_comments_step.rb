When('I leave a comment as {string}, with email address {string}') do |username, email|
  within('#new_comment') do
    fill_in 'Name', with: username
    fill_in 'Email', with: email
    fill_in 'Comment', with: 'Some content'

    click_on 'Save'
  end
end

When('I leave a comment') do
  within('#new_comment') do
    fill_in 'Comment', with: 'Some content'

    click_on 'Save'
  end
end

When('I leave a comment, filling the bot field') do
  within('#new_comment') do
    fill_in 'Name', with: 'superbot'
    fill_in 'Email', with: 'superbot@spiders.example.com'
    fill_in 'Comment', with: 'Some content'
    fill_in 'Body', with: 'Some bot content'

    click_on 'Save'
  end
end

Then('I should see the comment posted by {string}') do |string|
  within('.comments-list') do
    expect(current_scope).to have_content(string)
    expect(current_scope).to have_css('.comment', count: 1)
  end
end

Then('I should not see any comment') do
  expect(page).to have_css('.comment', count: 0)
end
