class DeleteCommentNotifications < ActiveRecord::Migration[5.2]
  def change
    Elabs::Notification.where(event: :comment).destroy_all
  end
end
