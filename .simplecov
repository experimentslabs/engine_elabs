# vim:filetype=ruby:

SimpleCov.start 'rails' do
  add_filter %r{/vendor}
end
