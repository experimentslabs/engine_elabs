module Elabs
  module ApplicationHelper
    def random_string(length = 5)
      [*('a'..'z'), *('0'..'9')].shuffle[0, length].join
    end

    def show_nsfw?
      session[:show_nsfw]
    end

    def page_title(title, namespace = nil)
      out = [Elabs.site_name]
      out.push(namespace) if namespace
      out.push(title) if title

      out.join('::')
    end

    # @author viphe https://stackoverflow.com/users/437585/viphe
    # This method yields a block.
    def with_format(format)
      old_formats = formats
      begin
        self.formats = [format]
        yield
      ensure
        self.formats = old_formats
      end
    end

    def singular_type_name(entity)
      entity.class.name.demodulize.tableize.singularize
    end

    def model_from_entity(type)
      "Elabs::#{type.classify}".constantize
    end
  end
end
