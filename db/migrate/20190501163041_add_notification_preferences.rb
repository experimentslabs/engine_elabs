class AddNotificationPreferences < ActiveRecord::Migration[5.2]
  def change
    add_column :preferences, :receive_comment_notifications, :boolean, null: false, default: true
    add_column :preferences, :receive_content_notifications, :boolean, null: false, default: true
    add_column :preferences, :receive_admin_notifications,   :boolean, null: false, default: true
  end
end
