# README

Rails engine adding CMS features to publish content related to projects.
An online version can be tested on http://next.experimentslabs.com

## Features

Manage various content types:

- Articles
- Notes
- Files
- Albums (files collections)
- Projects (may have everything above attached to them)

Classify content:

- Tags
- Language
- Licenses

Generic features:

- Comments (only visible to author)
- Safe/not safe for work content flags
- Content reporting
- Site structure translations

Features wishlist: check the [roadmap](ROADMAP.md#wonderland) to see what's coming.

## Status

Elabs engine is not production ready. If the core feature are working,
a lot of changes and improvements are still coming in. Use it in production
only if you don't mind to update often.

## Documentation:

- [Setup guide](docs/setup.md)
- [Shortcodes guide](docs/shortcodes_guide.md)
- [Sample app](https://gitlab.com/experimentslabs/sample_app)
- [How to override the app](docs/overriding.md)
- [How to contribute](docs/contributing.md)
- [Code maintainers](docs/CODEOWNERS) on specific subjects
- [Migration guide](docs/migrating.md)
- [Cucumber: terms used in features](docs/cucumber_terms.md)
- [Roadmap](ROADMAP.md): incoming changes
- [Changelog](CHANGELOG.md): last validated changes
- [Issue tracker](https://gitlab.com/experimentslabs/engine_elabs/issues)
- [Discussion place: Gitter](https://gitter.im/Experiments-Labs/Lobby)
