module Elabs
  class NotesTag < ApplicationRecord
    self.table_name = 'notes_tags'

    include Elabs::Concerns::CountablePivot

    COUNTABLE_DEFINITION = %i[notes tag].freeze

    belongs_to :note
    belongs_to :tag
  end
end
